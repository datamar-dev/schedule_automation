import os
import tkinter as tk
from requests import get
from time import sleep, time
from Input_Output import inandoutfile, merge_excel_files
from Insertion import insertion

root = tk.Tk()                                                  # initializing tkinter window
result = 0
filename = 'Schedules File.xlsx'
file_list = []
variable = tk.StringVar(root)
start_time = time()
agentname = ''
carrier_list = ['COSCO', 'PIL', 'YANGMING', 'GRIMALDI', 'NILE DUTCH', 'MSC', 'EVERGREEN', 'CMA CGM', 'HYUNDAI MERCHANT',
                'MAERSK/SEALAND/SAFMARINE', 'ZIM', 'HAPAG', 'ONE', 'MERCOSUL LINE'] # 'HAMBURG SUD']


def clear_widget_text(widget):
    widget['text'] = " "


# to select carrier from dropdown menu
def agentdropdown(*args):
    global agentname
    agentname = variable.get()
    return agentname


# to display output file path on tkinter window
def Generate_Excel():
    global agentname
    clear = tk.StringVar()
    clear.set(" ")
    if agentname == '':
        label5 = tk.Label(text="Please select the Carrier Name from the dropdown menu", fg="white", bg="black")
        label5.place(x=50, y=400)
        btn = tk.Button(root, text="Ok", command=lambda: clear_widget_text(label5))
        btn.place(x=400, y=400)
    if agentname != '':
        output = inandoutfile(agentname)
        if output == -1:
            label3 = tk.Label(text="Error in Tracking. Please click on Track Website button", fg="white", bg="black")
            label3.place(x=50, y=400)
            btn = tk.Button(root, text="Ok", command=lambda: clear_widget_text(label3))
            btn.place(x=400, y=400)
        elif output == 1:
            label4 = tk.Label(text="Excel file Generated", fg="white", bg="black")
            label4.place(x=50, y=400)


def Merge_Excels():
    value = merge_excel_files(filename)
    if value == -1:
        label4 = tk.Label(text="Error in merging excel files", fg="white", bg="black")
        label4.place(x=50, y=400)
    else:
        label4 = tk.Label(text="Excel file Merged    ", fg="white", bg="black")
        label4.place(x=50, y=400)


# to display output file path on tkinter window
def Deitram_Insertion():
    clear = tk.StringVar()
    clear.set(" ")
    output = insertion(filename)
    if output == 0:
        label3 = tk.Label(text="Error in Insertion. Please check " + filename + "", fg="white", bg="black")
        label3.place(x=50, y=400)
        btn = tk.Button(root, text="Ok", command=lambda: clear_widget_text(label3))
        btn.place(x=400, y=400)
    elif output == -1:
        label3 = tk.Label(text="" + filename + " is not available", fg="white", bg="black")
        label3.place(x=50, y=400)
        btn = tk.Button(root, text="Ok", command=lambda: clear_widget_text(label3))
        btn.place(x=400, y=400)
    elif output == 1:
        label4 = tk.Label(text="Insertion into Deitram completed", fg="white", bg="black")
        label4.place(x=50, y=430)


# initializing labels, entryboxes, buttons for tkinter window
label2 = tk.Label(text="Select the Carrier", fg="white", bg="black")
label2.place(x=100, y=80)

variable.set('      ')        # default value
dropdown = tk.OptionMenu(root, variable, *carrier_list)
dropdown.place(x=100, y=110)
variable.trace('w', agentdropdown)

button = tk.Button(root, text="Track Websites and\n Generate Excel", width=20, command=Generate_Excel)
button.place(x=50, y=240)

button = tk.Button(root, text="Merge all Excel files", width=20, command=Merge_Excels)
button.place(x=250, y=250)

button = tk.Button(root, text="Insert to Deitram", width=20, command=Deitram_Insertion)
button.place(x=150, y=320)


# driver code
if __name__ == '__main__':
    root.geometry('500x500')
    root.title("Schedules v1.0")
    root.mainloop()
