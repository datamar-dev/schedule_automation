import os
import shutil
import pandas as pd
from Cosco import cosco
from Pil_Schedule import pil
from YangMing import yangming
from Grimaldi import grimaldi
from Nile_Dutch import nile_dutch
from Evergreen import evergreen
from CMA_CGM import cma_cgm
from Maersk import maersk
from MSC import msc
from Zim import zim
from Hyundai_Merchant import hyundai
from Hapag import hapag
from One import one
from Mercosul_Line import mercosul
#from Hamburg_sud import hamburg_sud


file_dict = {'COSCO': 'Cosco.xlsx', 'PIL': 'Pil.xlsx', 'YANGMING': 'Yangming.xlsx', 'GRIMALDI': 'Grimaldi.xlsx',
             'NILE DUTCH': 'Nile.xlsx', 'MSC': 'Msc.xlsx', 'EVERGREEN': 'Evergreen.xlsx', 'CMA CGM': 'Cgm.xlsx',
             'HYUNDAI MERCHANT': 'Hyundai.xlsx', 'MAERSK': 'Maersk.xlsx', 'SEALAND': 'Sealand.xlsx',
             'SAFMARINE': 'Safmarine.xlsx', 'HAPAG': 'Hapag.xlsx', 'ZIM': 'Zim.xlsx', 'ONE': 'One.xlsx',
             'MERCOSUL LINE': 'Mercosul.xlsx', 'HAMBURG SUD': 'Hamburg.xlsx'}


def inandoutfile(carrier_name):
    try:
        for carrier, filename in file_dict.items():
            if carrier_name == 'COSCO' and carrier == 'COSCO':
                cosco(filename)
            elif carrier_name == 'PIL' and carrier == 'PIL':
                pil(filename)
            elif carrier_name == 'YANGMING' and carrier == 'YANGMING':
                yangming(filename)
            elif carrier_name == 'GRIMALDI' and carrier == 'GRIMALDI':
                grimaldi(filename)
            elif carrier_name == 'NILE DUTCH' and carrier == 'NILE DUTCH':
                nile_dutch(filename)
            elif carrier_name == 'EVERGREEN' and carrier == 'EVERGREEN':
                evergreen(filename)
            elif carrier_name == 'CMA CGM' and carrier == 'CMA CGM':
                cma_cgm(filename)
            elif carrier_name == 'MAERSK' and carrier == 'MAERSK':
                maersk(filename)
            elif carrier_name == 'MSC' and carrier == 'MSC':
                msc(filename)
            elif carrier_name == 'ZIM' and carrier == 'ZIM':
                zim(filename)
            elif carrier_name == 'HYUNDAI MERCHANT' and carrier == 'HYUNDAI MERCHANT':
                hyundai(filename)
            elif carrier_name == 'HAPAG' and carrier == 'HAPAG':
                hapag(filename)
            elif carrier_name == 'ONE' and carrier == 'ONE':
                one(filename)
            elif carrier_name == 'MERCOSUL LINE' and carrier == 'MERCOSUL LINE':
                mercosul(filename)
            #elif carrier_name == 'HAMBURG SUD' and carrier == 'HAMBURG SUD':
                #hamburg_sud(filename)
    except:
        return -1
    return 1


def merge_excel_files(file_name):
    directory = "Schedule_files"
    old_path = os.getcwd()
    new_path = os.path.join(os.getcwd(), directory)
    try:
        files = os.listdir(new_path)
        for f in files:
            os.chdir(new_path)
            os.remove(f)
        os.chdir(old_path)
        os.rmdir(directory)
    except:
        pass
    try:
        df = pd.DataFrame()
        os.mkdir(new_path)
        file_list = os.listdir(os.getcwd())
        for carrier, file in file_dict.items():
            if file in file_list:
                src = os.getcwd() + "\\" + file
                dst = new_path + "\\" + file
                shutil.move(src, dst)
        files = os.listdir(new_path)
        os.chdir(new_path)
        for each_file in files:
            df1 = pd.read_excel(open(each_file, 'rb'))
            df = pd.concat([df, df1])
        df.to_excel(file_name, index=False)
    except:
        return -1
    return 1
