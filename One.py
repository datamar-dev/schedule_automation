import os
import re
import time
import pandas as pd
from selenium import webdriver
from fake_useragent import UserAgent
from datetime import timedelta, datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementClickInterceptedException, ElementNotInteractableException


scheduled_country = ['ARGENTINA', 'BRAZIL', 'PARAGUAY', 'URUGUAY']
Argentina_dict = ['BUENOS AIRES', 'ZARATE', 'ROSARIO']
Paraguay_dict = ['ASUNCION', 'PILAR']
Brazil_dict = ['SANTOS', 'PARANAGUA', 'NAVEGANTES', 'ITAJAI', 'ITAPOA', 'BELEM', 'FORTALEZA', 'IMBITUBA', 'ITAGUAI', 'RECIFE',
               'ITAQUI', 'MANAUS', 'PECEM', 'PONTA DA MADEIRA', 'SAO FRANCISCO DO SUL', 'SALVADOR', 'SUAPE', 'VILA DO CONDE', 'VITORIA']
Uruguay_dict = ['MONTEVIDEO']
port_code_dict = {'BUENOS AIRES': 'BUE',  'ROSARIO': 'ROS', 'ZARATE': 'ZAE', 'MONTEVIDEO': 'MVD',
                   'IMBITUBA': 'IBB', 'ITAGUAI': 'IGI', 'ITAQUI': 'ITQ', 'ITAJAI': 'ITJ', 'ITAPOA': 'IOA',
                  'BELEM': 'BEL', 'FORTALEZA': 'FOR', 'MANAUS': 'MAO', 'PONTA DA MADEIRA': 'PMA', 'VILA DO CONDE': 'VLC',
                  'NAVEGANTES': 'NVT', 'PARANAGUA': 'PNG', 'RIO GRANDE': 'RIG', 'RIO DE JANEIRO': 'RIO',
                  'SANTOS': 'SSZ', 'PECEM': 'PEC', 'PORTO ALEGRE': 'POA', 'RECIFE': 'REC', 'SALVADOR': 'SSA',
                  'SAO FRANCISCO DO SUL': 'SFS', 'SEPETIBA': 'SPB', 'SUAPE': 'SUA', 'VITORIA': 'VIX'}
carrier_name_list, carrier_code_list, country_list, port_list, port_code_list, vessel_list, voyageimp_list, voyageexp_list, arrival_list, depart_list = [], [], [], [], [], [], [], [], [], []


def getDriver():
    chrome_options = Options()
    ua = UserAgent()
    user_agent = ua.random
    chrome_options.add_argument(f'user-agent={user_agent}')
    prefs = {'profile.managed_default_content_settings.images': 2}
    chrome_options.add_experimental_option("prefs", prefs)
    chrome_options.headless = True
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--ignore-ssl-errors')
    chrome_path = os.getcwd() + "\\chromedriver.exe"
    driver = webdriver.Chrome(chrome_path, options=chrome_options)
    driver.set_window_position(-10000, 0)
    return driver


def one(filename):
    carrier_name = 'ONE'
    carrier_code = '96663'
    today_date = datetime.today()
    last_date = today_date + timedelta(days=90)
    last_date = datetime.strptime(str(last_date), '%Y-%m-%d %H:%M:%S.%f').strftime('%Y-%m-%d')
    driver = getDriver()
    path = "https://ecomm.one-line.com/ecom/CUP_HOM_3006.do?sessLocale=en"
    driver.get(path)
    time.sleep(10)
    # clicking on the port entry tab
    driver.find_element(By.CSS_SELECTOR, '#portNm').click()
    port_selection = driver.find_element(By.CSS_SELECTOR, '#portNm')
    time.sleep(2)
    flag = 0
    port_code = ''
    for country in scheduled_country:
        if country == 'ARGENTINA':
            schedule_port = Argentina_dict
        elif country == 'BRAZIL':
            schedule_port = Brazil_dict
        elif country == 'PARAGUAY':
            schedule_port = Paraguay_dict
        else:
            schedule_port = Uruguay_dict
        for port in schedule_port:
            port_selection.clear()
            time.sleep(2)
            port_selection.send_keys(port)
            time.sleep(10)
            try:
                drop_down_selection = driver.find_elements(By.XPATH, '/html/body/ul/li')
                for drop in drop_down_selection:
                    if str(drop.text).__contains__('BRAZIL') or str(drop.text).__contains__('ARGENTINA')\
                            or str(drop.text).__contains__('PARAGUAY') or str(drop.text).__contains__('URUGUAY'):
                        drop.click()
                        break
                time.sleep(10)
                # selecting ocean vessel and selecting the last date
                if flag == 0:
                    driver.find_element(By.XPATH, '//*[@id="modFlgO"]').click()
                    time.sleep(2)
                    # selecting the last date
                    driver.find_element(By.XPATH, '//*[@id="frm"]/table[1]/tbody/tr[3]/td/div/button[2]').click()
                    time.sleep(3)
                    date_selection = driver.find_element(By.XPATH,
                                                         '/html/body/div[3]/div[2]/div/form/table[1]/tbody/tr[3]/td/div/input[2]')
                    date_selection.send_keys(str(last_date))
                    time.sleep(3)
                    flag = 1
                # Clicking on search button
                driver.find_element(By.XPATH, '//*[@id="btnSearch"]/span').click()
                time.sleep(10)
                all_data = driver.find_elements(By.XPATH, '//*[@id="skdTb"]/tbody/tr')
                for data in all_data:
                    try:
                        ves_voy = data.find_element(By.CSS_SELECTOR, 'td:nth-child(2)').text
                        vessel = re.split(r'\d{1}', ves_voy)[0].rstrip(' FI')
                        voyage = str(ves_voy).split(vessel)[-1].strip()
                        arr = data.find_element(By.CSS_SELECTOR, 'td:nth-child(5)').text
                        arrival = str(arr).split(' ')[0]
                        dep = data.find_element(By.CSS_SELECTOR, 'td:nth-child(7)').text
                        departure = str(dep).split(' ')[0]
                        if str(voyage).__contains__('E') or str(voyage).__contains__('R') or str(voyage).__contains__('N'):
                            voyageexp_list.append(voyage)
                            voyageimp_list.append(' ')
                        elif str(voyage).__contains__('W') or str(voyage).__contains__('S') or str(voyage).__contains__('A'):
                            voyageimp_list.append(voyage)
                            voyageexp_list.append(' ')
                        else:
                            voyageimp_list.append(voyage)
                            voyageexp_list.append(voyage)
                        for p_name, p_code in port_code_dict.items():
                            if port == p_name:
                                port_code = p_code
                        vessel_list.append(vessel)
                        arrival_list.append(arrival)
                        depart_list.append(departure)
                        port_list.append(port)
                        port_code_list.append(port_code)
                        country_list.append(country)
                        carrier_name_list.append(carrier_name)
                        carrier_code_list.append(carrier_code)
                    except:
                        continue
            except:
                continue
    driver.close()

    df = pd.DataFrame()
    df['CARRIER'] = carrier_name_list
    df['CARRIER CODE'] = carrier_code_list
    df['COUNTRY'] = pd.Series(country_list)
    df['PORT'] = pd.Series(port_list)
    df['PORT CODE'] = pd.Series(port_code_list)
    df['VESSEL NAME'] = pd.Series(vessel_list)
    df['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df['ARRIVAL DATE'] = pd.Series(arrival_list)
    df['DEPARTURE DATE'] = pd.Series(depart_list)
    today_date = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f').strftime('%d %b %Y')
    df['LAST EXECUTED'] = today_date
    df.to_excel(filename, index=False)
