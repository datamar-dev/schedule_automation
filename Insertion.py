import openpyxl
import pandas as pd
import pyodbc
import xml.etree.ElementTree as ET
from pydantic.datetime_parse import timedelta
from datetime import datetime

# initializing SQL connection
tree = ET.parse('server.xml')
root = tree.getroot()
server_tag = root.find('SERVER')
server = server_tag.get('SERVER')
username = server_tag.get('USERNAME')
password = server_tag.get('PASSWORD')
driverSQL = server_tag.get('DRIVERSQL')
Trusted_connection = server_tag.get('Trusted_connection')

conn1 = pyodbc.connect(
    'DRIVER=' + driverSQL + ';SERVER=' + server + ';Trusted_connection=' + Trusted_connection + ';UID=' + username + ';PWD=' + password)
excursor = conn1.cursor()


def insertion(filename):
    try:
        df = pd.read_excel(open(filename, 'rb'))
    except:
        return -1
    workbook = openpyxl.load_workbook(filename)
    sheet = workbook.active
    c_row = 1
    sheet.cell(c_row, 12, 'Comment')
    c_row += 1
    data = ''
    for item in df.values:
        try:
            port_code = str(item[4])
            carrier_code = str(item[1])
            vessel = str(item[5])
            eta = item[8]
            diff = timedelta(days=5)
            try:
                start_date = (datetime.strptime(eta, '%Y-%m-%d') - diff).strftime('%Y%m%d')
                end_date = (datetime.strptime(eta, '%Y-%m-%d') + diff).strftime('%Y%m%d')
            except:
                if str(item[8]).__contains__('nan') or str(item[8]).__contains__(' '):
                    data = 'NO ARRIVAL DATE IN EXCEL'
                sheet.cell(c_row, 12, data)
                c_row += 1
                continue

            # Checking voyage in Excel
            if str(item[6]).__contains__('nan') and str(item[7]).__contains__('nan') or str(item[6]).__contains__(' ') and str(item[7]).__contains__(' ') \
                    or str(item[6]).__contains__(' ') and str(item[7]).__contains__('nan') or str(item[6]).__contains__('nan') and str(item[7]).__contains__(' '):
                sheet.cell(c_row, 12, 'NO VOYAGES IN EXCEL')
                c_row += 1
                continue

            # Vessel not found Query
            vessel_not_found = excursor.execute("SELECT t1.DC_NAVIO FROM [Deitram02].[dbo].[NAVIO] as t1 "
                                                "INNER JOIN [Deitram02].[dbo].[MOVPORT] as t2 on t1.ID_Navio_DataLiner = t2.ID_Navio_DataLiner "
                                                "INNER JOIN [Deitram02].[dbo].[OPERACAO_TEMP] as t3 on t2.Id_MovPort = t3.ID_MovPort "
                                                "WHERE t1.DC_NAVIO = '" + vessel + "' ").fetchone()
            if vessel_not_found is None:
                sheet.cell(c_row, 12, 'VESSEL NOT FOUND')
                c_row += 1
                continue

            # Carrier not found Query
            carrier_not_found = excursor.execute("SELECT t1.DC_NAVIO FROM [Deitram02].[dbo].[NAVIO] as t1 "
                                                 "INNER JOIN [Deitram02].[dbo].[MOVPORT] as t2 on t1.ID_Navio_DataLiner = t2.ID_Navio_DataLiner "
                                                 "INNER JOIN [Deitram02].[dbo].[OPERACAO_TEMP] as t3 on t2.Id_MovPort = t3.ID_MovPort  and t3.CD_ARMADOR = " + carrier_code + " "
                                                 "WHERE t1.DC_NAVIO = '" + vessel + "' ").fetchone()
            if carrier_not_found is None:
                sheet.cell(c_row, 12, 'CARRIER NOT FOUND')
                c_row += 1
                continue

            # port not found Query
            port_not_found = excursor.execute("SELECT t1.DC_NAVIO, t3.CD_ARMADOR FROM [Deitram02].[dbo].[NAVIO] as t1 "
                                              "INNER JOIN [Deitram02].[dbo].[MOVPORT] as t2 on t1.ID_Navio_DataLiner = t2.ID_Navio_DataLiner and t2.cd_porto = '" + port_code + "' "
                                              "INNER JOIN [Deitram02].[dbo].[OPERACAO_TEMP] as t3 on t2.Id_MovPort = t3.ID_MovPort and t3.CD_ARMADOR = " + carrier_code + " "
                                              "WHERE t1.DC_NAVIO = '" + vessel + "' ").fetchone()
            if port_not_found is None:
                sheet.cell(c_row, 12, 'PORT NOT FOUND')
                c_row += 1
                continue

            # Arrival not found Query
            arrival_not_found = excursor.execute("SELECT t3.OPERACAO, t3.VIAGEM FROM [Deitram02].[dbo].[NAVIO] as t1 "
                                                 "INNER JOIN [Deitram02].[dbo].[MOVPORT] as t2 on t1.ID_Navio_DataLiner = t2.ID_Navio_DataLiner and t2.cd_porto = '" + port_code + "' "
                                                 "INNER JOIN [Deitram02].[dbo].[OPERACAO_TEMP] as t3 on t2.Id_MovPort = t3.ID_MovPort and t3.CD_ARMADOR = " + carrier_code + " "
                                                 "WHERE t1.DC_NAVIO = '" + vessel + "' "
                                                 "AND t2.Dt_EstimadaChegada >= '" + start_date + "' AND t2.Dt_EstimadaChegada < '" + end_date + "' ").fetchone()
            if arrival_not_found is None:
                sheet.cell(c_row, 12, 'ARRIVAL DATE NOT FOUND')
                c_row += 1
                continue

            voy_exp_check = excursor.execute("SELECT t3.OPERACAO, t3.VIAGEM FROM [Deitram02].[dbo].[NAVIO] as t1 "
                                             "INNER JOIN [Deitram02].[dbo].[MOVPORT] as t2 on t1.ID_Navio_DataLiner = t2.ID_Navio_DataLiner and t2.cd_porto = '" + port_code + "' "
                                             "INNER JOIN [Deitram02].[dbo].[OPERACAO_TEMP] as t3 on t2.Id_MovPort = t3.ID_MovPort and t3.CD_ARMADOR = " + carrier_code + " "
                                             "WHERE t1.DC_NAVIO = '" + vessel + "' "
                                             "AND t2.Dt_EstimadaChegada >= '" + start_date + "' AND t2.Dt_EstimadaChegada < '" + end_date + "' "
                                             "AND t3.OPERACAO = 1").fetchone()

            voy_imp_check = excursor.execute("SELECT t3.OPERACAO, t3.VIAGEM FROM [Deitram02].[dbo].[NAVIO] as t1 "
                                             "INNER JOIN [Deitram02].[dbo].[MOVPORT] as t2 on t1.ID_Navio_DataLiner = t2.ID_Navio_DataLiner and t2.cd_porto = '" + port_code + "' "
                                             "INNER JOIN [Deitram02].[dbo].[OPERACAO_TEMP] as t3 on t2.Id_MovPort = t3.ID_MovPort and t3.CD_ARMADOR = " + carrier_code + " "
                                             "WHERE t1.DC_NAVIO = '" + vessel + "' "
                                             "AND t2.Dt_EstimadaChegada >= '" + start_date + "' AND t2.Dt_EstimadaChegada < '" + end_date + "' "
                                             "AND t3.OPERACAO = 2").fetchone()

        except:
            sheet.cell(c_row, 12, 'ERROR WHILE CHECKING INFO')
            c_row += 1
            continue

        flag = 0
        if isinstance(voy_exp_check, type(None)):
            flag = 1

        if isinstance(voy_imp_check, type(None)):
            flag = 2

        if flag == 0:
            if not str(voy_exp_check[1]).__contains__('          ') and not str(voy_imp_check[1]).__contains__('          ') or \
             not str(voy_exp_check[1]).__contains__('          ') or not str(voy_imp_check[1]).__contains__('          '):
                sheet.cell(c_row, 12, 'VOYAGE ALREADY EXISTS')
                c_row += 1
                continue

        elif flag == 1:
            if not str(voy_imp_check[1]).__contains__('          '):
                sheet.cell(c_row, 12, 'VOYAGE ALREADY EXISTS')
                c_row += 1
                continue

        elif flag == 2:
            if not str(voy_exp_check[1]).__contains__('          '):
                sheet.cell(c_row, 12, 'VOYAGE ALREADY EXISTS')
                c_row += 1
                continue
                
        try:
            try:
                if flag == 0:
                    excursor.execute(";with cte as (SELECT t3.OPERACAO, t3.VIAGEM FROM [Deitram02].[dbo].[NAVIO] as t1 "
                                "INNER JOIN [Deitram02].[dbo].[MOVPORT] as t2 on t1.ID_Navio_DataLiner = t2.ID_Navio_DataLiner and t2.cd_porto = '" + port_code + "' "
                                 "INNER JOIN [Deitram02].[dbo].[OPERACAO_TEMP] as t3 on t2.Id_MovPort = t3.ID_MovPort and t3.CD_ARMADOR = " + carrier_code + " "
                                 "WHERE t1.DC_NAVIO = '" + vessel + "' "
                                 "AND t2.Dt_EstimadaChegada >= '" + start_date + "' AND t2.Dt_EstimadaChegada < '" + end_date + "' "
                                 "AND t3.OPERACAO = 1) update cte set VIAGEM = '" + str(item[6]) + "'")

                    excursor.execute(";with cte as (SELECT t3.OPERACAO, t3.VIAGEM FROM [Deitram02].[dbo].[NAVIO] as t1 "
                                "INNER JOIN [Deitram02].[dbo].[MOVPORT] as t2 on t1.ID_Navio_DataLiner = t2.ID_Navio_DataLiner and t2.cd_porto = '" + port_code + "' "
                                "INNER JOIN [Deitram02].[dbo].[OPERACAO_TEMP] as t3 on t2.Id_MovPort = t3.ID_MovPort and t3.CD_ARMADOR = " + carrier_code + " "
                                "WHERE t1.DC_NAVIO = '" + vessel + "' "
                                "AND t2.Dt_EstimadaChegada >= '" + start_date + "' AND t2.Dt_EstimadaChegada < '" + end_date + "' "
                                "AND t3.OPERACAO = 2) update cte set VIAGEM = '" + str(item[7]) + "'")

                elif flag == 1:
                    excursor.execute(";with cte as (SELECT t3.OPERACAO, t3.VIAGEM FROM [Deitram02].[dbo].[NAVIO] as t1 "
                                     "INNER JOIN [Deitram02].[dbo].[MOVPORT] as t2 on t1.ID_Navio_DataLiner = t2.ID_Navio_DataLiner and t2.cd_porto = '" + port_code + "' "
                                     "INNER JOIN [Deitram02].[dbo].[OPERACAO_TEMP] as t3 on t2.Id_MovPort = t3.ID_MovPort and t3.CD_ARMADOR = " + carrier_code + " "
                                     "WHERE t1.DC_NAVIO = '" + vessel + "' "
                                     "AND t2.Dt_EstimadaChegada >= '" + start_date + "' AND t2.Dt_EstimadaChegada < '" + end_date + "' "
                                     "AND t3.OPERACAO = 2) update cte set VIAGEM = '" + str(item[7]) + "'")

                elif flag == 2:
                    excursor.execute(";with cte as (SELECT t3.OPERACAO, t3.VIAGEM FROM [Deitram02].[dbo].[NAVIO] as t1 "
                                     "INNER JOIN [Deitram02].[dbo].[MOVPORT] as t2 on t1.ID_Navio_DataLiner = t2.ID_Navio_DataLiner and t2.cd_porto = '" + port_code + "' "
                                     "INNER JOIN [Deitram02].[dbo].[OPERACAO_TEMP] as t3 on t2.Id_MovPort = t3.ID_MovPort and t3.CD_ARMADOR = " + carrier_code + " "
                                     "WHERE t1.DC_NAVIO = '" + vessel + "' "
                                     "AND t2.Dt_EstimadaChegada >= '" + start_date + "' AND t2.Dt_EstimadaChegada < '" + end_date + "' "
                                     "AND t3.OPERACAO = 1) update cte set VIAGEM = '" + str(item[6]) + "'")

                excursor.execute(";with cte as (SELECT t3.OPERACAO, t3.VIAGEM FROM [Deitram02].[dbo].[NAVIO] as t1 "
                                 "INNER JOIN [Deitram02].[dbo].[MOVPORT] as t2 on t1.ID_Navio_DataLiner = t2.ID_Navio_DataLiner and t2.cd_porto = '" + port_code + "' "
                                 "INNER JOIN [Deitram02].[dbo].[OPERACAO_TEMP] as t3 on t2.Id_MovPort = t3.ID_MovPort and t3.CD_ARMADOR = " + carrier_code + " "
                                 "WHERE t1.DC_NAVIO = '" + vessel + "' "
                                 "AND t2.Dt_EstimadaChegada >= '" + start_date + "' AND t2.Dt_EstimadaChegada < '" + end_date + "' "
                                 "AND t3.OPERACAO = 1) update cte set VIAGEM = '" + str(item[6]) + "'")

                excursor.execute(";with cte as (SELECT t3.OPERACAO, t3.VIAGEM FROM [Deitram02].[dbo].[NAVIO] as t1 "
                                 "INNER JOIN [Deitram02].[dbo].[MOVPORT] as t2 on t1.ID_Navio_DataLiner = t2.ID_Navio_DataLiner and t2.cd_porto = '" + port_code + "' "
                                 "INNER JOIN [Deitram02].[dbo].[OPERACAO_TEMP] as t3 on t2.Id_MovPort = t3.ID_MovPort and t3.CD_ARMADOR = " + carrier_code + " "
                                 "WHERE t1.DC_NAVIO = '" + vessel + "' "
                                 "AND t2.Dt_EstimadaChegada >= '" + start_date + "' AND t2.Dt_EstimadaChegada < '" + end_date + "' "
                                 "AND t3.OPERACAO = 2) update cte set VIAGEM = '" + str(item[7]) + "'")
                sheet.cell(c_row, 12, 'VOYAGE UPDATED')
                c_row += 1

            except TypeError:
                continue

            conn1.commit()
        except:
            sheet.cell(c_row, 12, 'ERROR WHILE UPDATING')
            c_row += 1
            continue
    workbook.save(filename)
    return 1
