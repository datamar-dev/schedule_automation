import os
import re
import sys
import time
import pandas as pd
import csv
from datetime import timedelta, datetime
from openpyxl import Workbook
from selenium import webdriver
from fake_useragent import UserAgent
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementClickInterceptedException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from threading import Lock
from concurrent.futures import ThreadPoolExecutor


carrier_name_list, carrier_code_list, country_list, port_list, port_code_list, vessel_list, voyageimp_list, voyageexp_list, arrival_list, depart_list = [], [], [], [], [], [], [], [], [], []
all_scheduled_ports = ['BUENOS AIRES', 'MONTEVIDEO', 'ITAPOA', 'ITAJAI, SC', 'NAVEGANTES, SC', 'PARANAGUA', 'RIO GRANDE',
                   'RIO DE JANEIRO', 'VITORIA, ES', 'SALVADOR', 'SANTOS', 'PORT OF ITAGUAI']
scheduled_port1 = ['BUENOS AIRES',]
scheduled_port2 = ['ITAPOA', 'ITAJAI, SC', 'NAVEGANTES, SC', 'PARANAGUA', 'RIO GRANDE',
                   'RIO DE JANEIRO', 'VITORIA, ES', 'SALVADOR', 'SANTOS', 'PORT OF ITAGUAI']
scheduled_port3 = ['MONTEVIDEO']
port_code_dict = {'BUENOS AIRES': 'BUE', 'CONCEPCION DEL URUGUAY': 'CON', 'CORRIENTES': 'CNQ', 'LA PLATA': 'LPG', 'PUERTO MADRYN': 'PMY',
                  'ROSARIO': 'ROS', 'USHUAIA': 'USH', 'ZARATE': 'ZAE', 'BELEM': 'BEL', 'FORTALEZA': 'FOR', 'IMBITUBA': 'IBB',
                  'PORT OF ITAGUAI': 'IGI', 'ITAJAI, SC': 'ITJ', 'ITAPOA': 'IOA', 'ITAQUI': 'ITQ', 'MACAPA': 'MCP',
                  'MACEIO': 'MCZ', 'MANAUS': 'MAO', 'NATAL': 'NAT', 'NAVEGANTES, SC': 'NVT', 'PARANAGUA': 'PNG', 'PECEM': 'PEC',
                  'PORTO ALEGRE': 'POA', 'RECIFE': 'REC', 'RIO DE JANEIRO': 'RIO', 'RIO GRANDE': 'RIG', 'SALVADOR': 'SSA',
                  'SANTANA': 'SAN', 'SANTAREM': 'STM', 'SANTOS': 'SSZ',  'SAO FRANCISCO DO SUL': 'SFS', 'SUAPE': 'SUA',
                  'VILA DO CONDE': 'VCO', 'VITORIA, ES': 'VIX', 'MONTEVIDEO': 'MVD', 'BAHIA BLANCA': 'BHI', 'Mar del Plata': 'MDQ',
                  'Puerto Deseado': 'PUD'}


# initialising Chrome Driver
def getDriver():
    chrome_options = Options()
    ua = UserAgent()
    user_agent = ua.random
    chrome_options.add_argument(f'user-agent={user_agent}')
    prefs = {'profile.managed_default_content_settings.images': 2}
    chrome_options.add_experimental_option("prefs", prefs)
    chrome_options.headless = True
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--ignore-ssl-errors')
    chrome_options.add_experimental_option('prefs', {
        "download.default_directory": os.getcwd(),  # Change default directory for downloads
        "download.prompt_for_download": False,  # To auto download the file
        "download.directory_upgrade": True,
        "plugins.always_open_pdf_externally": True  # It will not show PDF directly in chrome
    })
    chrome_path = os.getcwd() + "\\chromedriver.exe"
    driver = webdriver.Chrome(chrome_path, options=chrome_options)
    driver.set_window_position(-10000, 0)
    return driver


def formatting_arrival(arrival):
    today = datetime.today()
    current_month = today.month
    year = today.year
    arr_month = str(arrival).split(' ')[-1]
    month1 = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun']
    month2 = ['Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    if current_month >= 10:
        if arr_month in month1:
            year_new = str(year + 1)
            arrival = arrival + ' ' + year_new
        else:
            arrival = arrival + ' ' + str(year)
    elif current_month <= 5:
        if arr_month in month2:
            year_new = str(year - 1)
            arrival = arrival + ' ' + year_new
        else:
            arrival = arrival + ' ' + str(year)
    else:
        arrival = arrival + ' ' + str(year)
    arrival = datetime.strptime(str(arrival), '%d %b %Y').strftime('%Y-%m-%d')
    return arrival


def fetching_each_set(carrier_name, carrier_code, new_vessel_list, new_voyage_list, new_port_list, new_date_list, check_port):
    dummy_ves_list, dummy_voy_list, dummy_port_list, dummy_date_list = [], [], [], []
    port_code = ' '
    for po, da in zip(range(0, len(new_port_list), len(new_vessel_list)), range(0, len(new_date_list), len(new_vessel_list))):
        i = 0
        append_port_list, append_date_list = [], []
        while i < len(new_vessel_list):
            append_port_list.append(new_port_list[po + i])
            append_date_list.append(new_date_list[da + i])
            i = i + 1
        dummy_port_list.append(append_port_list)
        dummy_date_list.append(append_date_list)
        dummy_ves_list.append(new_vessel_list)
        dummy_voy_list.append(new_voyage_list)
    for vessels, voyages, ports, arr_dates in zip(dummy_ves_list, dummy_voy_list, dummy_port_list, dummy_date_list):
        for vessel, voyage, port, arrival in zip(vessels, voyages, ports, arr_dates):
            carrier_name_list.append(carrier_name)
            carrier_code_list.append(carrier_code)
            if port in scheduled_port1:
                country = 'ARGENTINA'
            elif port in scheduled_port2:
                country = 'BRAZIL'
            else:
                country = 'URUGUAY'
            country_list.append(country)
            vessel_list.append(vessel)
            if check_port == 'import':
                voyageexp_list.append(' ')
                voyageimp_list.append(voyage)
            else:
                voyageexp_list.append(voyage)
                voyageimp_list.append(' ')
            port_list.append(port)
            for port_key, port_value in port_code_dict.items():
                if port == port_key:
                    port_code = port_value
            port_code_list.append(port_code)
            if str(arrival).__contains__('nan') or str(arrival).__contains__('OMIT'):
                arrival_list.append(' ')
            else:
                arrival = formatting_arrival(arrival)
                arrival_list.append(arrival)
            depart_list.append(' ')
    return check_port


def hapag(filename):
    carrier_name = 'HAPAG'
    carrier_code = '1813'
    today_date = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f').strftime('%d %b %Y')
    driver = getDriver()
    path = "https://www.hapag-lloyd.com/en/online-business/schedule/schedule-download-solution.html"
    driver.get(path)
    time.sleep(2)
    driver.find_element_by_id("accept-recommended-btn-handler").click()
    time.sleep(2)
    classname1 = driver.find_element_by_class_name("boxContent")
    classname1.find_element_by_css_selector("table>tbody>tr>td>table>tbody>tr>td>table>tbody>tr:nth-child(1)>td:nth-child(2)>table>tbody>tr>td>table>tbody>tr>td>div>img").click()
    time.sleep(2)
    selection1 = driver.find_elements_by_class_name("x-combo-list-item")
    count = 0
    for each in selection1:
        if count == 3:
            each.click()
            break
        count += 1
    time.sleep(2)
    classname1.find_element_by_css_selector("table>tbody>tr>td>table>tbody>tr>td>table>tbody>tr:nth-child(3)>td:nth-child(2)>table>tbody>tr>td>table>tbody>tr>td>div>img").click()
    for each in selection1:
        if count == 0:
            each.click()
            break
    time.sleep(2)
    classname2 = driver.find_element_by_class_name("buttonPanelRight")
    classname2.find_element_by_css_selector("table>tbody>tr>td:nth-child(1)>button:nth-child(1)").click()
    time.sleep(2)
    classname3 = driver.find_element_by_css_selector("#table-stripe>table>tbody>tr>td>table>tbody>tr>td>table>tbody>tr>td>table")
    classname3.find_element_by_css_selector("tbody>tr:nth-child(2)>td").click()
    time.sleep(2)
    classname3.find_element_by_css_selector("tfoot>tr>td:nth-child(2)>button").click()
    time.sleep(2)
    driver.close()

    # fetching second file
    driver = getDriver()
    path = "https://www.hapag-lloyd.com/en/online-business/schedule/schedule-download-solution.html"
    driver.get(path)
    time.sleep(2)
    driver.find_element_by_id("accept-recommended-btn-handler").click()
    time.sleep(2)
    classname1 = driver.find_element_by_class_name("boxContent")
    classname1.find_element_by_css_selector("table>tbody>tr>td>table>tbody>tr>td>table>tbody>tr:nth-child(1)>td:nth-child(2)>table>tbody>tr>td>table>tbody>tr>td>div>img").click()
    time.sleep(2)
    selection1 = driver.find_elements_by_class_name("x-combo-list-item")
    count = 0
    for each in selection1:
        if count == 0:
            each.click()
            break
    time.sleep(2)
    classname1.find_element_by_css_selector("table>tbody>tr>td>table>tbody>tr>td>table>tbody>tr:nth-child(3)>td:nth-child(2)>table>tbody>tr>td>table>tbody>tr>td>div>img").click()
    for each in selection1:
        if count == 9:
            each.click()
            break
        count += 1
    time.sleep(2)
    classname2 = driver.find_element_by_class_name("buttonPanelRight")
    classname2.find_element_by_css_selector("table>tbody>tr>td:nth-child(1)>button:nth-child(1)").click()
    time.sleep(2)
    classname3 = driver.find_element_by_css_selector(
        "#table-stripe>table>tbody>tr>td>table>tbody>tr>td>table>tbody>tr>td>table")
    classname3.find_element_by_css_selector("tbody>tr:nth-child(2)>td").click()
    time.sleep(2)
    classname3.find_element_by_css_selector("tfoot>tr>td:nth-child(2)>button").click()
    time.sleep(2)
    driver.close()

    workbook = Workbook()
    worksheet = workbook.active
    files = ["SCHEDULE-LA-AS.csv", "SCHEDULE-AS-LA.csv"]
    excel_files = []
    check_port = ''
    for file in files:
        with open(file, 'r') as f:
            for row in csv.reader(f):
                worksheet.append(row)
        if file == "SCHEDULE-LA-AS.csv":
            output_file = 'Export.xlsx'
        else:
            output_file = 'Import.xlsx'
        excel_files.append(output_file)
        workbook.save(output_file)
    for each_file in excel_files:
        if each_file == 'Import.xlsx':
            check_port = 'import'
        elif each_file == 'Export.xlsx':
            check_port = 'export'
        df = pd.read_excel(open(each_file, 'rb'))
        flag = 0
        all_data = []
        vessel_name_list = []
        for item in df.values:
            voyages = []
            vessels = []
            port_dates = []
            if flag == 0:
                vessel_name_list = []
                if str(item).__contains__('DATE:') or str(item).__contains__('TIME:') or str(item).__contains__('PAGE:')\
                        or str(item).__contains__('EDP-NO.') or str(item).__contains__('LOOP/SERVICE') or str(item).__contains__('LOADING')\
                        or str(item).__contains__('DISCHARGING') or str(item).count('nan') == 8:
                    continue
                elif str(item).__contains__('VESSEL-NAME'):
                    vessel_name_list.append(str(item))
                    flag = 1
                else:
                    for each in item:
                        port_dates.append(str(each).replace('+', '').replace('.', '').strip(' '))
                    all_data.append(port_dates)
            elif flag == 1:
                if str(item).__contains__('VOY.NO'):
                    voy = str(item).replace('\n', '').replace("'VOY.NO'", '"\'VOY.NO\'"').replace('nan', '"\'nan\'"').split('" "')
                    for v1 in voy:
                        voyages.append(v1.replace('[', '').replace(']', '').replace("'", '').replace('"', ''))
                    new_i = vessel_name_list[0].replace('nan', "'nan'").replace("['", '').replace("']", '').replace("\n", '').split("' '")
                    new_j = vessel_name_list[1].replace("[", '').replace("]", '').replace("\n", '').split(" ")
                    for i, j in zip(range(0, len(new_i)), range(0, len(new_j))):
                        value = new_i[i].strip("'") + ' ' + new_j[j].strip("'")
                        vessels.append(value.replace('nan', '').strip(' '))
                    all_data.append(vessels)
                    all_data.append(voyages)
                    flag = 0
                else:
                    vessel_name_list.append(str(item))
        new_vessel_list, new_voyage_list, new_port_list, new_date_list = [], [], [], []
        splitting_flag = 0
        for each_line in all_data:
            if splitting_flag == 0:
                if each_line[0].__contains__('VESSEL-NAME'):
                    for each_data1 in each_line:
                        if each_data1.__contains__('VESSEL-NAME'):
                            continue
                        new_vessel_list.append(each_data1)
                    splitting_flag = 1
            elif splitting_flag == 1:
                counter = 0
                while 1:
                    if each_line[0].__contains__('VOY.NO'):
                        for each_data2 in each_line:
                            if each_data2.__contains__('VOY.NO'):
                                continue
                            new_voyage_list.append(each_data2)
                        break
                    elif not each_line[0].__contains__('VOY.NO'):
                        flag = 0
                        port_name = ''
                        for each_data3 in each_line:
                            if flag == 0:
                                if each_data3 not in all_scheduled_ports:
                                    break
                                port_name = each_data3
                                flag = 1
                            elif flag == 1:
                                new_port_list.append(port_name)
                                new_date_list.append(each_data3)
                                counter += 1
                        if each_line[0] not in all_scheduled_ports:
                            break
                    if len(each_line) - 1 == counter:
                        break
                if each_line[0].__contains__('VESSEL-NAME'):
                    check_port = fetching_each_set(carrier_name, carrier_code, new_vessel_list, new_voyage_list, new_port_list, new_date_list, check_port)
                    new_vessel_list, new_voyage_list, new_port_list, new_date_list = [], [], [], []
                    for each_data1 in each_line:
                        if each_data1.__contains__('VESSEL-NAME'):
                            continue
                        new_vessel_list.append(each_data1)
                    splitting_flag = 1
        check_port = fetching_each_set(carrier_name, carrier_code, new_vessel_list, new_voyage_list, new_port_list, new_date_list, check_port)
        os.remove(each_file)
    os.remove("SCHEDULE-LA-AS.csv")
    os.remove("SCHEDULE-AS-LA.csv")
    df = pd.DataFrame()
    df['CARRIER'] = carrier_name_list
    df['CARRIER CODE'] = carrier_code_list
    df['COUNTRY'] = pd.Series(country_list)
    df['PORT'] = pd.Series(port_list)
    df['PORT CODE'] = pd.Series(port_code_list)
    df['VESSEL NAME'] = pd.Series(vessel_list)
    df['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df['ARRIVAL DATE'] = pd.Series(arrival_list)
    df['DEPARTURE DATE'] = pd.Series(depart_list)
    df['LAST EXECUTED'] = today_date
    df.to_excel(filename, index=False)
