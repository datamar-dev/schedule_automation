import os
import re
import sys
import time
from datetime import timedelta, datetime
import pandas as pd
from selenium import webdriver
from fake_useragent import UserAgent
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementClickInterceptedException
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC
from threading import Lock
from concurrent.futures import ThreadPoolExecutor

port_dict = {'BUENOS AIRES': 'ARGENTINA', 'ITAJAI': 'BRAZIL', 'ITAPOA': 'BRAZIL', 'NAVEGANTES': 'BRAZIL',
             'PARANAGUA': 'BRAZIL', 'RIO GRANDE': 'BRAZIL', 'SANTOS': 'BRAZIL', 'MONTEVIDEO': 'URUGUAY'}
port_code_dict = {'BAHIA BLANCA': 'BHI', 'BUENOS AIRES': 'BUE', 'CONCEPCION DEL URUGUAY': 'CON', 'CORRIENTES': 'CNQ',
                  'ROSARIO': 'ROS', 'ZARATE': 'ZAR', 'BELEM': 'BEL', 'FORTALEZA': 'FOR', 'IMBITUBA': 'IBB',
                  'ITAGUAI': 'IGI', 'ITAPOA': 'IOA', 'ITAJAI': 'ITJ', 'ITAQUI': 'ITQ', 'MACAPA': 'MCP',
                  'MACEIO': 'MCZ', 'MANAUS': 'MAO', 'NATAL': 'NAT', 'NAVEGANTES': 'NVT', 'PARANAGUA': 'PNG', 'PECEM': 'PEC',
                  'PORTO ALEGRE': 'POA', 'RECIFE': 'REC', 'RIO DE JANEIRO': 'RIO', 'RIO DE JANIERO': 'RIO', 'RIO GRANDE': 'RIG',
                  'SALVADOR DI BAHIA': 'SSA', 'SAO PAULO': 'SPO', 'SANTANA': 'SAN', 'SANTAREM': 'STM', 'SANTOS': 'SSZ',
                  'SAO FRANCISCO DO SUL': 'SFS', 'SUAPE': 'SUA', 'VILA DO CONDE': 'VCO', 'VITORIA': 'VIX', 'MONTEVIDEO': 'MVD'}

carrier_name_list, carrier_code_list, country_list, port_list, port_code_list, vessel_list, voyageimp_list, voyageexp_list, arrival_list, depart_list = [], [], [], [], [], [], [], [], [], []
paths = ["https://www.niledutch.com/en/schedules-tracking/sailing-schedules/east-coast-south-america-africa?Schedule=7",
         "https://www.niledutch.com/en/schedules-tracking/sailing-schedules/africa-east-coast-south-america?Schedule=8"]


# initialising Chrome Driver
def getDriver():
    chrome_options = Options()
    ua = UserAgent()
    user_agent = ua.random
    chrome_options.add_argument(f'user-agent={user_agent}')
    prefs = {'profile.managed_default_content_settings.images': 2}
    chrome_options.add_experimental_option("prefs", prefs)
    chrome_options.headless = True
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--ignore-ssl-errors')
    chrome_path = os.getcwd() + "\\chromedriver.exe"
    driver = webdriver.Chrome(chrome_path, options=chrome_options)
    driver.set_window_position(-10000, 0)
    return driver


def nile_dutch(filename):
    carrier_name = 'NILE DUTCH'
    carrier_code = '63854'
    today_date = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f').strftime('%d %b %Y')
    driver = getDriver()
    port_code = ''
    for path in paths:
        driver.get(path)
        time.sleep(2)
        tables_name = driver.find_elements_by_class_name("styleTable")
        tables_data = driver.find_elements_by_class_name("dataTable")
        for each_table_name, each_table_data in zip(tables_name, tables_data):
            voyage = each_table_data.find_element_by_css_selector("tbody > tr:nth-child(1) > th").text
            vessel = each_table_data.find_element_by_css_selector("thead > tr").text
            name_details = each_table_name.find_elements_by_css_selector("tbody > tr > td")
            data_details = each_table_data.find_elements_by_css_selector("tbody > tr > td")
            for port_name, arrival_data in zip(name_details, data_details):
                for port, country in port_dict.items():
                    if str(port_name.text).strip(' ') == port:
                        for name, code in port_code_dict.items():
                            if name == port:
                                port_code = code
                        port_list.append(port)
                        country_list.append(country)
                        arrival = str(arrival_data.text).split(' ')[-1]
                        arrival_date = datetime.strptime(str(arrival), '%b-%d').strftime('%d/%m')
                        today = datetime.today()
                        year = today.year
                        arr_month = str(arrival_date).split('/')[1]
                        if arr_month == '01':
                            year_new = str(year + 1)
                            arrival = str(arrival_date) + '/' + year_new
                        else:
                            arrival = str(arrival_date) + '/' + str(year)
                        arrival = datetime.strptime(arrival, '%d/%m/%Y').strftime('%Y-%m-%d')
                        arrival_list.append(arrival)
                        depart_list.append(' ')
                        vessel_list.append(vessel)
                        port_code_list.append(port_code)
                        carrier_name_list.append(carrier_name)
                        carrier_code_list.append(carrier_code)
                        if voyage[-1] == 'E' or voyage[-1] == 'N' or voyage[-1] == 'R':
                            voyageexp_list.append(voyage)
                            voyageimp_list.append(' ')
                        else:
                            voyageexp_list.append(' ')
                            voyageimp_list.append(voyage)
        time.sleep(5)
    driver.close()

    df = pd.DataFrame()
    df['CARRIER'] = carrier_name_list
    df['CARRIER CODE'] = carrier_code_list
    df['COUNTRY'] = pd.Series(country_list)
    df['PORT'] = pd.Series(port_list)
    df['PORT CODE'] = pd.Series(port_code_list)
    df['VESSEL NAME'] = pd.Series(vessel_list)
    df['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df['ARRIVAL DATE'] = pd.Series(arrival_list)
    df['DEPARTURE DATE'] = pd.Series(depart_list)
    df['LAST EXECUTED'] = today_date
    df.to_excel(filename, index=False)
