import os
import sys
import time
from datetime import timedelta, datetime
import pandas as pd
from selenium import webdriver
from fake_useragent import UserAgent
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementClickInterceptedException, ElementNotInteractableException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from threading import Lock
from concurrent.futures import ThreadPoolExecutor

# lock = Lock()
carrier_name_list, carrier_code_list, country_list, port_list, port_code_list, vessel_list, voyageimp_list, voyageexp_list, arrival_list, depart_list = [], [], [], [], [], [], [], [], [], []
scheduled_list1 = ['Buenos aires', 'Montevideo', 'Navegantes', 'Paranagua', 'Rio Grande',
                   'Rio de Janeiro', 'Santos']
scheduled_list2 = ['Itaguai', 'Itajai', 'Itapoa', 'Navegantes', 'Paranagua', 'Santos']
port_code_dict = {'Buenos aires': 'BUE', 'Montevideo': 'MVD', 'Itaguai': 'IGI', 'Itajai': 'ITJ', 'Itapoa': 'IOA',
                  'Navegantes': 'NVT', 'Paranagua': 'PNG', 'Rio Grande': 'RIG', 'Rio de Janeiro': 'RIO',
                  'Santos': 'SSZ'}


# initialising Chrome Driver
def getDriver():
    chrome_options = Options()
    ua = UserAgent()
    user_agent = ua.random
    chrome_options.add_argument(f'user-agent={user_agent}')
    prefs = {'profile.managed_default_content_settings.images': 2}
    chrome_options.add_experimental_option("prefs", prefs)
    chrome_options.headless = True
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--ignore-ssl-errors')
    chrome_path = os.getcwd() + "\\chromedriver.exe"
    driver = webdriver.Chrome(chrome_path, options=chrome_options)
    driver.set_window_position(-10000, 0)
    return driver


def cosco(filename):
    carrier_name = 'COSCO'
    carrier_code = '2160'
    today_date = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f').strftime('%d %b %Y')
    driver = getDriver()
    path_links = ["body>div:nth-child(4)>div>div>div>div>div>div>div>div:nth-child(3)>div>div>div >div.ivu-collapse-item.ivu-collapse-item-active>div.ivu-collapse-content>div>ul>li:nth-child(1)",
        "body>div:nth-child(4)>div>div>div>div>div>div>div>div:nth-child(3)>div>div>div>div.ivu-collapse-item.ivu-collapse-item-active>div.ivu-collapse-content>div>ul>li:nth-child(2)",
        "body>div:nth-child(2)>div>div>div>div>div>div>div>div:nth-child(3)>div>div>div>div.ivu-collapse-item.ivu-collapse-item-active>div.ivu-collapse-content>div>ul>li:nth-child(2)"]
    flag = 0
    for each_path in path_links:
        path = "https://elines.coscoshipping.com/ebusiness/sailingSchedule/searchByService"
        driver.get(path)
        driver.delete_all_cookies()
        # cookies
        try:
            driver.find_element_by_css_selector("body>div:nth-child(13)>div>div>div>div>div>button").click()
        except Exception:
            try:
                driver.find_element_by_css_selector("body>div:nth-child(11)>div>div>div>div>div>button").click()
            except:
                time.sleep(2)
        time.sleep(5)
        selector = driver.find_element_by_css_selector("body>div>div>div>div>div>div>div>div")
        selector.find_element_by_css_selector("div:nth-child(1)>div:nth-child(5)").click()
        time.sleep(2)
        selector.find_element_by_css_selector("div:nth-child(3)>div>div>div>div:nth-child(5)").click()
        time.sleep(2)
        try:
            driver.find_element_by_css_selector(each_path).click()
        except:
            continue
        time.sleep(2)
        try:
            driver_selection = driver.find_element_by_css_selector("body>div:nth-child(4)>div.router-footer-glass-wrap>div.commonRouter")
        except:
            driver_selection = driver.find_element_by_css_selector("body>div:nth-child(2)>div.router-footer-glass-wrap>div.commonRouter")
        input = driver_selection.find_element_by_css_selector("div>div>div>div>form>div.ivu-col.ivu-col-span-20>div>div>div>div.ivu-select-selection>input")
        input.send_keys("Santos")
        time.sleep(2)
        dropdown = driver_selection.find_element_by_css_selector("div>div>div>div>form>div.ivu-col.ivu-col-span-20>div>div>div>div.ivu-select-dropdown>ul.ivu-select-dropdown-list>div>li")
        dropdown.click()
        time.sleep(2)
        driver_selection.find_element_by_css_selector("div>div>div>div>form>div.ivu-col.ivu-col-span-4>button").click()   ## search button click
        time.sleep(10)
        # selecting 8 weeks not happening as its automated chrome
        driver_selection.find_element_by_css_selector("div>div.filter>div.filter-bar>div>div:nth-child(5)>div>div>div.ivu-select-selection>i.ivu-icon.ivu-icon-chevron-down.ivu-select-arrow").click()
        time.sleep(10)
        all_ports = driver_selection.find_elements_by_css_selector("div>div.filter>div.filter-bar>div>div:nth-child(5)>div>div>div.ivu-select-dropdown>ul.ivu-select-dropdown-list>li")
        if flag == 0:
            scheduled_list = scheduled_list1
        else:
            scheduled_list = scheduled_list2
        port_code, arrival, depart = '', '', ''
        for port in scheduled_list:
            for scheduled_port in all_ports:
                if str(scheduled_port.text) == port:
                    scheduled_port.click()
                    time.sleep(5)
                    all_data = driver.find_elements_by_css_selector("#capture>div:nth-child(2)>div>div>div")
                    flag = 1
                    for data in all_data:
                        vessel_voyage = data.find_element_by_css_selector("div>div>div.filter-header>div>p:nth-child(2)").text
                        vessel = vessel_voyage.split('(Vessel:')[-1].strip(')').strip(' ')
                        voyage = str(vessel_voyage).split('(')[0].strip('Voyage ').split('/')
                        if port == 'Buenos aires':
                            country = 'ARGENTINA'
                        elif port == 'Montevideo':
                            country = 'URUGUAY'
                        else:
                            country = 'BRAZIL'
                        for name, code in port_code_dict.items():
                            if name == port:
                                port_code = code

                        arrivals = data.find_elements_by_css_selector(
                            "div>div>div>div>div>div>div>table>tbody>tr>td:nth-child(2)>div>div")
                        departs = data.find_elements_by_css_selector(
                            "div>div>div>div>div>div>div>table>tbody>tr>td:nth-child(4)>div>div")
                        for arr, dep in zip(arrivals, departs):
                            arrival = arr.text.replace('\n', ' ').split(' ')[0]
                            depart = dep.text.replace('\n', ' ').split(' ')[0]
                            carrier_name_list.append(carrier_name)
                            carrier_code_list.append(carrier_code)
                            arrival_list.append(arrival)
                            depart_list.append(depart)
                            country_list.append(country)
                            port_list.append(port.upper())
                            port_code_list.append(port_code)
                            vessel_list.append(vessel)
                            voyageexp_list.append(voyage[1])
                            voyageimp_list.append(voyage[0])
            driver_selection.find_element_by_css_selector(
                "div>div.filter>div.filter-bar>div>div:nth-child(5)>div>div>div.ivu-select-selection>i.ivu-icon.ivu-icon-chevron-down.ivu-select-arrow").click()
            time.sleep(2)
            all_ports = driver_selection.find_elements_by_css_selector(
                "div>div.filter>div.filter-bar>div>div:nth-child(5)>div>div>div.ivu-select-dropdown>ul.ivu-select-dropdown-list>li")
    time.sleep(5)
    driver.close()

    df = pd.DataFrame()
    df['CARRIER'] = carrier_name_list
    df['CARRIER CODE'] = carrier_code_list
    df['COUNTRY'] = pd.Series(country_list)
    df['PORT'] = pd.Series(port_list)
    df['PORT CODE'] = pd.Series(port_code_list)
    df['VESSEL NAME'] = pd.Series(vessel_list)
    df['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df['ARRIVAL DATE'] = pd.Series(arrival_list)
    df['DEPARTURE DATE'] = pd.Series(depart_list)
    df['LAST EXECUTED'] = today_date
    df.to_excel(filename, index=False)
