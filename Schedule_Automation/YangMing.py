import os
import sys
import time
from datetime import timedelta, datetime
import pandas as pd
from selenium import webdriver
from fake_useragent import UserAgent
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementClickInterceptedException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from threading import Lock
from concurrent.futures import ThreadPoolExecutor

#lock = Lock()
schedule_country = ['ARGENTINA', 'BRAZIL', 'URUGUAY']
Argentina_port_dict = {'BUENOS AIRES': 'BUENOS AIRES (ARBUE)'}
Brazil_port_dict = {'NAVEGANTES': 'NAVEGANTES (BRNVT)', 'PARANAGUA': 'PARANAGUA (BRPNG)', 'RIO DE JANEIRO': 'RIO DE JANEIRO (BRRIO)',
         'RIO GRANDE': 'RIO GRANDE (BRRIG)', 'SANTOS': 'SANTOS (BRSSZ)'}
Uruguay_port_dict = {'MONTEVIDEO': 'MONTEVIDEO (UYMVD)'}
port_code_dict = {'BUENOS AIRES': 'BUE', 'ROSARIO': 'ROS', 'ITAGUAI': 'IGI', 'ITAPOA': 'IOA', 'ITAJAI': 'ITJ',
                  'NAVEGANTES': 'NVT', 'PARANAGUA': 'PNG', 'RIO GRANDE': 'RIG', 'RIO DE JANEIRO': 'RIO',
                  'SAO PAULO': 'SPO', 'SANTOS': 'SSZ', 'MONTEVIDEO': 'MVD'}
carrier_name_list, carrier_code_list, country_list, port_list, port_code_list, vessel_list, voyageimp_list, voyageexp_list, arrival_list, depart_list = [], [], [], [], [], [], [], [], [], []


# initialising Chrome Driver
def getDriver():
    chrome_options = Options()
    ua = UserAgent()
    user_agent = ua.random
    chrome_options.add_argument(f'user-agent={user_agent}')
    #chrome_options.headless = True
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--ignore-ssl-errors')
    chrome_path = os.getcwd() + "\\chromedriver.exe"
    driver = webdriver.Chrome(chrome_path, options=chrome_options)
    driver.set_window_position(-10000, 0)
    return driver


def yangming(filename):
    carrier_name = 'YANGMING'
    carrier_code = '52302'
    today_date1 = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f').strftime('%d %b %Y')
    port_code = ''
    today_date = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f')
    start_date = today_date.strftime('%Y/%m/%d')
    delay_date = (today_date + timedelta(days=90)).strftime('%Y/%m/%d')
    driver = getDriver()
    path = "https://www.yangming.com/e-service/track_trace/track_trace_cargo_tracking.aspx"
    driver.get(path)
    time.sleep(2)
    driver.find_element_by_css_selector("#menu_en>li:nth-child(7)>a").click()
    time.sleep(2)
    driver.find_element_by_css_selector("#menu_en>li:nth-child(7)>div>div>dl:nth-child(2)>dd:nth-child(3)>a").click()
    time.sleep(2)
    driver.find_element_by_class_name("cc-dismiss").click()
    time.sleep(2)
    date_from = driver.find_element_by_id("ContentPlaceHolder1_date_Start")
    date_from.clear()
    date_from.send_keys(start_date)
    date_to = driver.find_element_by_id("ContentPlaceHolder1_date_End")
    date_to.clear()
    date_to.send_keys(delay_date)
    time.sleep(2)
    for country in schedule_country:
        if country == 'ARGENTINA':
            schedule_port = Argentina_port_dict
        elif country == 'BRAZIL':
            schedule_port = Brazil_port_dict
        else:
            schedule_port = Uruguay_port_dict
        for key, port_value in schedule_port.items():
            port_selection = Select(driver.find_element_by_id("ContentPlaceHolder1_ddlCallingPort"))
            port_selection.select_by_visible_text(port_value)
            time.sleep(2)
            driver.find_element_by_id("ContentPlaceHolder1_btnSearch").click()
            time.sleep(3)
            all_data = driver.find_elements_by_css_selector("#ContentPlaceHolder1_gvMain>tbody>tr")
            for data in all_data:
                vessel = data.find_element_by_css_selector("td:nth-child(1)").text
                voyage = str(data.find_element_by_css_selector("td:nth-child(2)").text).split('\n')
                arrival = str(data.find_element_by_css_selector("td:nth-child(5)").text).split(' ')[0]
                depart = str(data.find_element_by_css_selector("td:nth-child(7)").text).split(' ')[0]
                arrival = datetime.strptime(str(arrival), '%Y/%m/%d').strftime('%Y-%m-%d')
                depart = datetime.strptime(str(depart), '%Y/%m/%d').strftime('%Y-%m-%d')
                vessel_list.append(vessel)
                if 'TBN' in voyage:
                    voyageimp_list.append(' ')
                    voyageexp_list.append(' ')
                elif len(voyage) == 1:
                    if voyage == "['']":
                        voyageimp_list.append(' ')
                        voyageexp_list.append(' ')
                    elif 'W' in str(voyage[0]):
                        voyageimp_list.append(voyage[0])
                        voyageexp_list.append(' ')
                    elif 'E' in str(voyage[0]):
                        voyageexp_list.append(voyage[0])
                        voyageimp_list.append(' ')
                elif len(voyage) > 1:
                    if str(voyage[0]) == str(voyage[1]):
                        if 'W' in str(voyage[0]):
                            voyageimp_list.append(voyage[0])
                            voyageexp_list.append(' ')
                        elif 'E' in str(voyage[0]):
                            voyageexp_list.append(voyage[0])
                            voyageimp_list.append(' ')
                    else:
                        if 'W' in str(voyage[0]):
                            voyageimp_list.append(voyage[0])
                            voyageexp_list.append(voyage[1])
                        else:
                            voyageexp_list.append(voyage[0])
                            voyageimp_list.append(voyage[1])
                arrival_list.append(arrival)
                depart_list.append(depart)
                country_list.append(country)
                port_list.append(key)
                carrier_name_list.append(carrier_name)
                carrier_code_list.append(carrier_code)
                for name, code in port_code_dict.items():
                    if name == key:
                        port_code = code
                port_code_list.append(port_code)
            time.sleep(2)
    driver.close()

    df = pd.DataFrame()
    df['CARRIER'] = carrier_name_list
    df['CARRIER CODE'] = carrier_code_list
    df['COUNTRY'] = pd.Series(country_list)
    df['PORT'] = pd.Series(port_list)
    df['PORT CODE'] = pd.Series(port_code_list)
    df['VESSEL NAME'] = pd.Series(vessel_list)
    df['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df['ARRIVAL DATE'] = pd.Series(arrival_list)
    df['DEPARTURE DATE'] = pd.Series(depart_list)
    df['LAST EXECUTED'] = today_date1
    df.to_excel(filename, index=False)
