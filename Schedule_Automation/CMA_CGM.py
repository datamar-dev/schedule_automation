import os
import sys
import time
from datetime import timedelta, datetime
import pandas as pd
from selenium import webdriver
from fake_useragent import UserAgent
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementClickInterceptedException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from threading import Lock
from concurrent.futures import ThreadPoolExecutor

schedule_country = ['ARGENTINA', 'BRAZIL', 'URUGUAY']
Argentina_port_list = ['BUENOS AIRES', 'CONCEPCION DEL URUGUAY', 'CORRIENTES', 'LA PLATA', 'PUERTO MADRYN', 'ROSARIO',
                       'USHUAIA', 'ZARATE']
Brazil_port_list = ['BELEM', 'FORTALEZA', 'IMBITUBA', 'ITAGUAI, RJ', 'ITAJAI', 'ITAPOA, SC', 'ITAQUI, MA', 'MACAPA',
                    'MACEIO', 'MANAUS', 'NATAL', 'NAVEGANTES', 'PARANAGUA', 'PECEM', 'PORTO ALEGRE', 'RECIFE',
                    'RIO DE JANEIRO', 'RIO GRANDE', 'SALVADOR DI BAHIA', 'SANTANA', 'SANTAREM', 'SANTOS',
                    'SAO FRANCISCO DO SUL', 'SUAPE', 'VILA DO CONDE', 'VITORIA']
Uruguay_port_list = ['MONTEVIDEO']
port_code_dict = {'BUENOS AIRES': 'BUE', 'CONCEPCION DEL URUGUAY': 'CON', 'CORRIENTES': 'CNQ', 'LA PLATA': 'LPG', 'PUERTO MADRYN': 'PMY',
                  'ROSARIO': 'ROS', 'USHUAIA': 'USH', 'ZARATE': 'ZAE', 'BELEM': 'BEL', 'FORTALEZA': 'FOR', 'IMBITUBA': 'IBB',
                  'ITAGUAI, RJ': 'IGI', 'ITAJAI': 'ITJ', 'ITAPOA, SC': 'IOA', 'ITAQUI, MA': 'ITQ', 'MACAPA': 'MCP',
                  'MACEIO': 'MCZ', 'MANAUS': 'MAO', 'NATAL': 'NAT', 'NAVEGANTES': 'NVT', 'PARANAGUA': 'PNG', 'PECEM': 'PEC',
                  'PORTO ALEGRE': 'POA', 'RECIFE': 'REC', 'RIO DE JANEIRO': 'RIO', 'RIO GRANDE': 'RIG', 'SALVADOR DI BAHIA': 'SSA',
                  'SANTANA': 'SAN', 'SANTAREM': 'STM', 'SANTOS': 'SSZ',  'SAO FRANCISCO DO SUL': 'SFS', 'SUAPE': 'SUA',
                  'VILA DO CONDE': 'VCO', 'VITORIA': 'VIX', 'MONTEVIDEO': 'MVD'}

carrier_name_list, carrier_code_list, country_list, port_list, port_code_list, vessel_list, voyageimp_list, voyageexp_list, arrival_list, depart_list = [], [], [], [], [], [], [], [], [], []


# initialising Chrome Driver
def getDriver():
    chrome_options = Options()
    ua = UserAgent()
    user_agent = ua.random
    chrome_options.add_argument(f'user-agent={user_agent}')
    prefs = {'profile.managed_default_content_settings.images': 2}
    chrome_options.add_experimental_option("prefs", prefs)
    chrome_options.headless = True
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--ignore-ssl-errors')
    chrome_path = os.getcwd() + "\\chromedriver.exe"
    driver = webdriver.Chrome(chrome_path, options=chrome_options)
    driver.set_window_position(-10000, 0)
    return driver


path_list = ['SEAS', 'SEAS2', 'BRASEX', 'NEFGUI', 'PLATAMRC', 'SIRIUS', 'SAFRAN1']


def data_fetch(driver, carrier_name, carrier_code):
    port_name, port_code, voyage, arrival, vessel, end_day = '', '', '', '', '', ''
    ves_list, voy_list, po_list, arr_list = [], [], [], []
    table_length = driver.find_elements_by_css_selector("#fullres-schedules > thead > tr > th > i")
    vessels = driver.find_elements_by_css_selector('#fullres-schedules>thead>tr>th>a')
    voyagg = driver.find_elements_by_class_name('first')
    ports = driver.find_elements_by_css_selector('#fullres-schedules > tbody > tr > td')
    checking = driver.find_elements_by_css_selector("#fullres-schedules > tbody > tr ")
    count = 0
    listing1, listing2 = [], []
    for check in checking:
        try:
            names = check.find_element_by_css_selector("td:nth-child(1)").text
            if count == 1:
                    listing1.append(names)
            else:
                listing2.append(names)
        except:
            count += 1
    newlisting1, newlisting2 = [], []
    for list1, list2 in zip(listing1, listing2):
        if list1 in Argentina_port_list or list1 in Brazil_port_list or list1 in Uruguay_port_list:
            newlisting1.append(list1)
        elif list2 in Argentina_port_list or list2 in Brazil_port_list or list2 in Uruguay_port_list:
            newlisting2.append(list2)
        else:
            continue
    country = ''
    new_voy1, new_voy2 = [], []
    flag = 0
    dummy_list = []
    for each_voy in voyagg:
        if flag == 0:
            new_voy1 = each_voy.find_elements_by_css_selector('#fullres-schedules>tbody>tr>th>a:nth-child(1)')
            flag = 1
        elif flag == 1:
            new_voy2 = each_voy.find_elements_by_css_selector('#fullres-schedules>tbody>tr>th>a:nth-child(1)')
    dummys = driver.find_elements_by_css_selector('#fullres-schedules > tbody > tr')
    for dummy1 in dummys:
        try:
            dummy_name = dummy1.find_element_by_css_selector("td > a").text
            if dummy_name in Argentina_port_list or dummy_name in Brazil_port_list or dummy_name in Uruguay_port_list:
                dummy_list.append(dummy_name)
        except:
            dummy_name = "Voyage"
            dummy_list.append(dummy_name)
    dummy_list = dummy_list[1:]
    for each in ports:
        try:
            port_name = each.find_element_by_css_selector("a").text
            if dummy_list[0] == port_name:
                dummy_list.pop(0)
            continue
        except:
            if port_name in Argentina_port_list or port_name in Brazil_port_list or port_name in Uruguay_port_list:
                try:
                    arrival = datetime.strptime(each.text, '%A, %B %d, %Y').strftime('%Y-%m-%d')
                except:
                    arrival = each.text
            else:
                continue
        po_list.append(port_name)
        arr_list.append(arrival)
        if dummy_list[0] == "Voyage":
            new_voy1 = new_voy2
        for vess, voyyy in zip(vessels, new_voy1):
            voyage = voyyy.text
            vessel = vess.text
            ves_list.append(vessel)
            voy_list.append(voyage)
    flag = 0
    port_dum_list, arr_dum_list, vess_dum_list, voy_dum_list = [], [], [], []
    for ve, vo in zip(range(0, len(ves_list), len(table_length)), range(0, len(voy_list), len(table_length))):
        sec_vess_list, sec_voy_list = [], []
        try:
            sec_vess_list.append(ves_list[ve])
            sec_vess_list.append(ves_list[ve+1])
            sec_vess_list.append(ves_list[ve+2])
            sec_vess_list.append(ves_list[ve+3])
        except:
            sec_vess_list.append(' ')
        try:
            sec_voy_list.append(voy_list[vo])
            sec_voy_list.append(voy_list[vo+1])
            sec_voy_list.append(voy_list[vo+2])
            sec_voy_list.append(voy_list[vo+3])
        except:
            sec_voy_list.append(' ')
        vess_dum_list.append(sec_vess_list)
        voy_dum_list.append(sec_voy_list)
    new_vess_dum_list, new_voy_dum_list = [], []
    for por, arr in zip(range(0, len(po_list), len(table_length)), range(0, len(arr_list), len(table_length))):
        sec_port_list, sec_arr_list, = [], []
        if flag == 0:
            try:
                sec_port_list.append(po_list[por])
                sec_port_list.append(po_list[por + 1])
                sec_port_list.append(po_list[por + 2])
                sec_port_list.append(po_list[por + 3])
            except:
                sec_port_list.append(' ')
            try:
                sec_arr_list.append(arr_list[arr])
                sec_arr_list.append(arr_list[arr + 1])
                sec_arr_list.append(arr_list[arr + 2])
                sec_arr_list.append(arr_list[arr + 3])
            except:
                sec_arr_list.append(' ')
            port_dum_list.append(sec_port_list)
            arr_dum_list.append(sec_arr_list)
            new_vess_dum_list.append(vess_dum_list[0])
            new_voy_dum_list.append(voy_dum_list[0])
            if len(port_dum_list) == len(newlisting1):
                flag = 1
        elif flag == 1:
            try:
                sec_port_list.append(po_list[por])
                sec_port_list.append(po_list[por + 1])
                sec_port_list.append(po_list[por + 2])
                sec_port_list.append(po_list[por + 3])
            except:
                sec_port_list.append(' ')
            try:
                sec_arr_list.append(arr_list[arr])
                sec_arr_list.append(arr_list[arr + 1])
                sec_arr_list.append(arr_list[arr + 2])
                sec_arr_list.append(arr_list[arr + 3])
            except:
                sec_arr_list.append(' ')
            port_dum_list.append(sec_port_list)
            arr_dum_list.append(sec_arr_list)
            try:
                new_vess_dum_list.append(vess_dum_list[-1])
            except:
                new_vess_dum_list.append(' ')
            try:
                new_voy_dum_list.append(voy_dum_list[-1])
            except:
                new_voy_dum_list.append(' ')
    for port_new_list, arrival_new_list, vessel_new_list, voyage_new_list in zip(port_dum_list, arr_dum_list, new_vess_dum_list, new_voy_dum_list):
        for port_new, arrival_new, vessel_new, voyage_new in zip(port_new_list, arrival_new_list, vessel_new_list, voyage_new_list):
            port_list.append(port_new)
            for port_key, port_value in port_code_dict.items():
                if port_key == port_new:
                    port_code = port_value
            port_code_list.append(port_code)
            if port_new in Argentina_port_list:
                country = 'ARGENTINA'
            elif port_new in Brazil_port_list:
                country = 'BRAZIL'
            elif port_new in Uruguay_port_list:
                country = 'URUGUAY'
            country_list.append(country)
            arrival_list.append(arrival_new)
            vessel_list.append(vessel_new)
            carrier_name_list.append(carrier_name)
            carrier_code_list.append(carrier_code)
            try:
                if str(voyage_new[5]) == 'W' or str(voyage_new[5]) == 'S' or str(voyage_new[5]) == 'A':
                    voyageimp_list.append(voyage_new)
                    voyageexp_list.append(' ')
                elif str(voyage_new[5]) == 'E' or str(voyage_new[5]) == 'N' or str(voyage_new[5]) == 'R':
                    voyageimp_list.append(' ')
                    voyageexp_list.append(voyage_new)
            except:
                continue
    return driver


def cma_cgm(filename):
    carrier_name = 'CMA CGM'
    carrier_code = '52914'
    today_date = datetime.today()
    driver = getDriver()
    path = "https://www.cma-cgm.com/products-services/line-services/schedules/"
    for service in path_list:
        new_path = path + service
        driver.get(new_path)
        driver.delete_all_cookies()
        time.sleep(10)
        driver = data_fetch(driver, carrier_name, carrier_code)
        while 1:
            try:
                driver.find_element_by_css_selector('#wrapper > div > div > div > button:nth-child(2)').click()
            except:
                try:
                    driver.find_element_by_css_selector('#wrapper > div > div > div > button').click()
                except:
                    continue
            driver = data_fetch(driver, carrier_name, carrier_code)
            url = driver.current_url.split('LastPageIndex=')[1]
            if str(url) == "3":
                break
    driver.close()

    df = pd.DataFrame()
    df['CARRIER'] = carrier_name_list
    df['CARRIER CODE'] = carrier_code_list
    df['COUNTRY'] = pd.Series(country_list)
    df['PORT'] = pd.Series(port_list)
    df['PORT CODE'] = pd.Series(port_code_list)
    df['VESSEL NAME'] = pd.Series(vessel_list)
    df['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df['ARRIVAL DATE'] = pd.Series(arrival_list)
    df['DEPARTURE DATE'] = pd.Series(depart_list)
    today_date = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f').strftime('%d %b %Y')
    df['LAST EXECUTED'] = today_date
    df.to_excel(filename, index=False)
