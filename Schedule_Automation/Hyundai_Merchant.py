import re
import os
import pdfplumber
import pandas as pd
from datetime import datetime


carrier_name_list, carrier_code_list, country_list, port_list, port_code_list, vessel_list, voyageimp_list, voyageexp_list, arrival_list, depart_list = [], [], [], [], [], [], [], [], [], []
scheduled_country = ['ARGENTINA', 'BRAZIL', 'URUGUAY']
Argentina_dict = {'BUENOS AIRES': 'HMM_Buenos Aires.pdf'}
Brazil_dict = {'ITAJAI': 'HMM_Itajai.pdf', 'ITAPOA': 'HMM_Itapoa.pdf', 'PARANAGUA': 'HMM_Paranagua.pdf', 'SANTOS': 'HMM_Santos.pdf'}
Uruguay_dict = {'MONTEVIDEO': 'HMM_Montevideo.pdf'}
port_code_dict = {'BUENOS AIRES': 'BUE', 'MONTEVIDEO': 'MVD', 'ITAJAI': 'ITJ', 'ITAPOA': 'IOA', 'PARANAGUA': 'PNG', 'SANTOS': 'SSZ'}


def hyundai(filename):
    carrier_name = 'HYUNDAI MERCHANT'
    carrier_code = '5690'
    files = os.listdir(os.getcwd())
    for country in scheduled_country:
        if country == 'ARGENTINA':
            schedule_port = Argentina_dict
        elif country == 'BRAZIL':
            schedule_port = Brazil_dict
        else:
            schedule_port = Uruguay_dict
        for port, file in schedule_port.items():
            if file in files:
                with pdfplumber.open(file) as pdf:
                    for page in pdf.pages:
                        text = page.extract_text()
                        for line in text.split('\n'):
                            checking = re.search(r' \d{4}-\d{2}-\d{2}', line)
                            if checking:
                                line = str(line).replace('', ' ')
                                arrival = checking.group().strip()
                                voyage = str(line).split(arrival)[0].split(' ')[-3]
                                vessel = str(line).split(arrival)[0].split(voyage)[0]
                                vessel = re.split(r'\d{1}', vessel)[-1].strip()
                                if str(voyage).__contains__('E') or str(voyage).__contains__('R') or str(voyage).__contains__('N'):
                                    voyageexp_list.append(voyage)
                                    voyageimp_list.append(' ')
                                else:
                                    voyageimp_list.append(voyage)
                                    voyageexp_list.append(' ')
                                for p_name, p_code in port_code_dict.items():
                                    if port == p_name:
                                        port_code = p_code
                                vessel_list.append(vessel)
                                arrival_list.append(arrival)
                                depart_list.append(' ')
                                port_list.append(port)
                                country_list.append(country)
                                port_code_list.append(port_code)
                                carrier_name_list.append(carrier_name)
                                carrier_code_list.append(carrier_code)

    df = pd.DataFrame()
    df['CARRIER'] = carrier_name_list
    df['CARRIER CODE'] = carrier_code_list
    df['COUNTRY'] = pd.Series(country_list)
    df['PORT'] = pd.Series(port_list)
    df['PORT CODE'] = pd.Series(port_code_list)
    df['VESSEL NAME'] = pd.Series(vessel_list)
    df['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df['ARRIVAL DATE'] = pd.Series(arrival_list)
    df['DEPARTURE DATE'] = pd.Series(depart_list)
    today_date = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f').strftime('%d %b %Y')
    df['LAST EXECUTED'] = today_date
    df.to_excel(filename, index=False)
