import os
import re
import sys
import time
from datetime import timedelta, datetime
import openpyxl
from openpyxl import Workbook
import csv
import pandas as pd
import tabula
from tabula import read_pdf
from PyPDF2 import PdfFileReader
from selenium import webdriver
from fake_useragent import UserAgent
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementClickInterceptedException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from threading import Lock
from concurrent.futures import ThreadPoolExecutor
from io import StringIO

# lock = Lock()

carrier_name_list, carrier_code_list, country_list, port_list, port_code_list, vessel_list, voyageimp_list, voyageexp_list, arrival_list, depart_list = [], [], [], [], [], [], [], [], [], []
port_code_dict = {'BUENOS AIRES': 'BUE', 'CONCEPCION DEL URUGUAY': 'CON', 'CORRIENTES': 'CNQ', 'LA PLATA': 'LPG', 'CAMPANA': 'CMP', 'PUERTO MADRYN': 'PMY',
                  'ROSARIO': 'ROS', 'USHUAIA': 'USH', 'ZARATE': 'ZAE', 'BELEM': 'BEL', 'FORTALEZA': 'FOR', 'IMBITUBA': 'IBB',
                  'ITAGUAI, RJ': 'IGI', 'ITAJAI': 'ITJ', 'ITAPOA': 'IOA', 'ITAQUI, MA': 'ITQ', 'MACAPA': 'MCP',
                  'MACEIO': 'MCZ', 'MANAUS': 'MAO', 'NATAL': 'NAT', 'NAVEGANTES': 'NVT', 'NAVEGENTES': 'NVT', 'PARANAGUA': 'PNG', 'PECEM': 'PEC',
                  'PORTO ALEGRE': 'POA', 'RECIFE': 'REC', 'RIO DE JANEIRO': 'RIO', 'RIO DE JANIERO': 'RIO', 'RIO GRANDE': 'RIG', 'SALVADOR': 'SSA',
                  'SANTANA': 'SAN', 'SANTAREM': 'STM', 'SANTOS': 'SSZ',  'SAO FRANCISCO DO SUL': 'SFS', 'SUAPE': 'SUA',
                  'VILA DO CONDE': 'VLC', 'VITORIA': 'VIX', 'MONTEVIDEO': 'MVD'}


# initialising Chrome Driver
def getDriver():
    chrome_options = Options()
    ua = UserAgent()
    user_agent = ua.random
    chrome_options.add_argument(f'user-agent={user_agent}')
    #chrome_options.headless = True
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--ignore-ssl-errors')
    chrome_options.add_experimental_option('prefs', {
        "download.default_directory": os.getcwd(), #Change default directory for downloads
        "download.prompt_for_download": False, #To auto download the file
        "download.directory_upgrade": True,
        "plugins.always_open_pdf_externally": True  # It will not show PDF directly in chrome
    })
    chrome_path = os.getcwd() + "\\chromedriver.exe"
    driver = webdriver.Chrome(chrome_path, options=chrome_options)
    #driver.set_window_position(-10000, 0)
    return driver


def msc_argentina(driver):
    carrier_name = 'MSC'
    carrier_code = '37702'
    port_code = ' '
    argentina_path = "https://www.msc.com/arg/help-centre/tools/vessel-s-position"
    driver.get(argentina_path)
    time.sleep(10)
    driver.find_element_by_class_name("js-reject").click()
    time.sleep(2)
    driver.find_element_by_id("onetrust-accept-btn-handler").click()
    time.sleep(5)
    driver.switch_to.frame("ctl00_ctl00_plcMain_plcMain_iframeContent")
    table = driver.find_element_by_class_name("pageContent")
    table1 = table.find_element_by_class_name("card-body")
    table2 = table1.find_element_by_css_selector("#form1>div:nth-child(8)>div:nth-child(5)")
    button = table2.find_element_by_xpath('//*[@id="btnExport"]')
    driver.execute_script("arguments[0].click();", button)
    time.sleep(15)
    tabula.convert_into("Schedule.pdf", "Dummy.csv", output_format="csv", pages="all")
    workbook = Workbook()
    worksheet = workbook.active
    with open("Dummy.csv", 'r') as f:
        for row in csv.reader(f):
            worksheet.append(row)
    workbook.save("Dummy.xlsx")
    os.remove("Dummy.csv")
    df = pd.read_excel(open("Dummy.xlsx", 'rb'))
    port = ''
    for item in df.values:
        if str(item[0]).__contains__('Buque'):
            continue
        elif str(item[0]).__contains__('Puerto:'):
            port = str(item[0]).split('Puerto:')[-1].split("' nan")[0].strip(' ')
            continue
        elif str(item[1]).__contains__('nan') and str(item[2]).__contains__('nan') and str(item[5]).__contains__('nan')\
                and str(item[6]).__contains__('nan'):
            continue
        else:
            item = str(item).replace("'", '').replace('\n', '')
            voyages = re.findall(r'\w*\d+\w*', item)
            voyageimp_list.append(voyages[0])
            if len(voyages[1]) >= 4 and voyages[1].isalnum():
                voyageexp_list.append(voyages[1])
            else:
                voyageexp_list.append(' ')
            vessel = item.split(voyages[0])[0].strip('[')
            vessel_list.append(vessel)
            all_dates = re.findall(r'\d+/\d+/\d+ \d+:\d+', item)
            arrival = datetime.strptime(all_dates[0], '%d/%m/%Y %H:%M').strftime('%Y-%m-%d')
            arrival_list.append(arrival)
            if len(all_dates) == 4 or len(all_dates) == 2:
                depart = datetime.strptime(all_dates[1], '%d/%m/%Y %H:%M').strftime('%Y-%m-%d')
                depart_list.append(depart)
            elif len(all_dates) == 3 or len(all_dates) == 1:
                depart_list.append(' ')
        for name, code in port_code_dict.items():
            if name == port:
                port_code = code
        port_list.append(port)
        port_code_list.append(port_code)
        country_list.append('ARGENTINA')
        carrier_name_list.append(carrier_name)
        carrier_code_list.append(carrier_code)
    os.remove("Dummy.xlsx")
    os.remove("Schedule.pdf")
    return driver


def arr_dep_formatting(arrival_date, depart_date):
    if arrival_date == '' and depart_date == '':
        return arrival_date, depart_date
    if 'RD ' in arrival_date:
        arrival = datetime.strptime(arrival_date, '%a %dRD %b %Y').strftime('%Y-%m-%d')
    elif 'ST ' in arrival_date:
        arrival = datetime.strptime(arrival_date, '%a %dST %b %Y').strftime('%Y-%m-%d')
    elif 'ND ' in arrival_date:
        arrival = datetime.strptime(arrival_date, '%a %dND %b %Y').strftime('%Y-%m-%d')
    elif 'TH ' in arrival_date:
        arrival = datetime.strptime(arrival_date, '%a %dTH %b %Y').strftime('%Y-%m-%d')
    else:
        arrival = ''
    if 'RD ' in depart_date:
        depart = datetime.strptime(depart_date, '%a %dRD %b %Y').strftime('%Y-%m-%d')
    elif 'ST ' in depart_date:
        depart = datetime.strptime(depart_date, '%a %dST %b %Y').strftime('%Y-%m-%d')
    elif 'ND ' in depart_date:
        depart = datetime.strptime(depart_date, '%a %dND %b %Y').strftime('%Y-%m-%d')
    elif 'TH ' in depart_date:
        depart = datetime.strptime(depart_date, '%a %dTH %b %Y').strftime('%Y-%m-%d')
    else:
        depart = ''
    return arrival, depart


def import_export_data(driver, scheduled_list1, scheduled_list2, carrier_name, carrier_code, country, flag):
    input1 = driver.find_element_by_css_selector("#app > div > div:nth-child(1) > div > div > div:nth-child(2) > div > div > div > div > input.port-input")
    input2 = driver.find_element_by_css_selector("#app > div > div:nth-child(1) > div > div > div:nth-child(4) > div > div > div > div > input.port-input")
    for country1 in scheduled_list1:
        input1.send_keys(Keys.BACK_SPACE)
        input1.send_keys(country1)
        time.sleep(4)
        try:
            driver.find_element_by_class_name("autocomplete__results__item").click()
            time.sleep(4)
        except:
            time.sleep(10)
            try:
                driver.find_element_by_class_name("autocomplete__results__item").click()
                time.sleep(4)
            except:
                continue
        for country2 in scheduled_list2:
            input2.send_keys(Keys.BACK_SPACE)
            input2.send_keys(country2)
            time.sleep(4)
            try:
                driver.find_element_by_class_name("autocomplete__results__item").click()
                time.sleep(5)
            except:
                time.sleep(10)
                try:
                    driver.find_element_by_class_name("autocomplete__results__item").click()
                    time.sleep(4)
                except:
                    continue
            try:
                driver.find_element_by_css_selector("#app > div > div:nth-child(1) > div > div > div:nth-child(7) > div > a.button.button--search.button-primary.reversed.expand").click()
                time.sleep(5)
            except:
                try:
                    time.sleep(5)
                    driver.find_element_by_css_selector("#app > div > div:nth-child(1) > div > div > div:nth-child(7) > div > a.button.button--search.button-primary.reversed.expand").click()
                    time.sleep(2)
                except:
                    continue
            try:
                driver.find_element_by_class_name("no-results")
                continue
            except:
                driver = fetching_data_Brazil(driver,  carrier_name, carrier_code, country, country1, country2, flag)
                while 1:
                    try:
                        driver.find_element_by_css_selector("#app > div > div:nth-child(2) > section > div > div > div > div > div > a.button.next").click()
                        time.sleep(2)
                        driver = fetching_data_Brazil(driver,  carrier_name, carrier_code, country, country1, country2, flag)
                        value = str(driver.find_element_by_css_selector("#app > div > div:nth-child(2) > section > div > div > div > div > div > span").text).split(' of ')
                        value1 = value[0]
                        value2 = value[1]
                        if value1 == value2:
                            break
                        else:
                            continue
                    except:
                        break
            time.sleep(5)
    flag = 1
    return driver, flag


def fetching_data_Brazil(driver,  carrier_name, carrier_code, country, country1, country2, flag):
    port, port_code = '', ''
    table_data = driver.find_elements_by_css_selector(
        "#app > div > div:nth-child(2) > section > div > div > div > div:nth-child(4) > div:nth-child(1) > div")
    for data in table_data:
        vess_voy = data.find_element_by_css_selector("div > div:nth-child(4) > div:nth-child(2)").text
        vv_split = str(vess_voy).split('/')
        vessel = vv_split[0].strip(' ')
        try:
            if flag == 0:
                port = country1
                voyageimp = ' '
                voyageexp = vv_split[1].strip(' ')
            else:
                port = country2
                voyageimp = vv_split[1].strip(' ')
                voyageexp = ' '
        except:
            voyageimp = ' '
            voyageexp = ' '
        try:
            arrival = data.find_element_by_css_selector("div > div:nth-child(3) > div:nth-child(2)").text
        except:
            arrival = ''
        try:
            depart = data.find_element_by_css_selector("div > div:nth-child(2) > div:nth-child(2)").text
        except:
            depart = ''
        arrival_date, depart_date = arr_dep_formatting(arrival, depart)
        vessel_list.append(vessel)
        for name, code in port_code_dict.items():
            if name == port:
                port_code = code
        port_list.append(port)
        port_code_list.append(port_code)
        voyageimp_list.append(voyageimp)
        voyageexp_list.append(voyageexp)
        arrival_list.append(arrival_date)
        depart_list.append(depart_date)
        carrier_name_list.append(carrier_name)
        carrier_code_list.append(carrier_code)
        country_list.append(country)
    return driver


def msc_brazil(driver):
    carrier_name = 'MSC'
    carrier_code = '37702'
    country = 'BRAZIL'
    scheduled_list1 = ['BELEM', 'ITAJAI', 'ITAPOA', 'MANAUS', 'NAVEGANTES', 'PARANAGUA', 'PECEM', 'PORTO VELHO',
                  'RIO GRANDE', 'RIO DE JANEIRO', 'SALVADOR', 'SANTAREM', 'SANTOS', 'SUAPE', 'VILA DO CONDE', 'VITORIA']
    scheduled_list2 = ['ALGECIRAS, SPAIN (ESALG)', 'CARTAGENA, COLOMBIA (COCTG)', 'CARTAGENA, SPAIN (ESCAR)', 'KINGSTON, JAMAICA (JMKIN)',
                     'NEW ORLEANS, UNITED STATES (USMSY)', 'HOUSTON, UNITED STATES (USHOU)', 'ALTAMIRA, MEXICO (MXATM)', 'VERACRUZ, MEXICO (MXVER)',
                    'MANZANILLO, PANAMA (PAMIT)', 'MANZANILLO, MEXICO (MXZLO)', 'COEGA, SOUTH AFRICA (ZAZBA)', 'SINGAPORE, SINGAPORE (SGSIN)',
                    'HONG KONG SAR, CHINA (HKHKG)', 'BUSAN, KOREA, REPUBLIC OF (KRPUS)', 'SHANGHAI, CHINA (CNSHA)', 'NINGBO, CHINA (CNNGB)',
                    'SHEKOU, CHINA (CNSHK)', 'YANTIAN, CHINA (CNYTN)', 'PORT KLANG (PELABUHAN KLANG), MALAYSIA (MYPKG)',
                    'ROTTERDAM, NETHERLANDS (NLRTM)', 'LONDON GATEWAY PORT, UNITED KINGDOM (GBLGP)', 'HAMBURG, GERMANY (DEHAM)',
                    'ANTWERP, BELGIUM (BEANR)', 'LE HAVRE, FRANCE (FRLEH)', 'CRISTOBAL, PANAMA (PACTB)', 'CALLAO, PERU (PECLL)',
                    'PORT EVERGLADES, UNITED STATES (USPEF)', 'GUAYAQUIL, ECUADOR (ECGYE)', 'ARICA, CHILE (CLARI)',
                    'SAN ANTONIO, CHILE (CLSAI)', 'SAN VICENTE, CHILE (CLSVE)', 'SINES, PORTUGAL (PTSIE)',
                    'BREMERHAVEN, GERMANY (DEBRV)', 'NEW YORK, UNITED STATES (USNYC)', 'PHILADELPHIA, UNITED STATES (USPHL)',
                    'NORFOLK, UNITED STATES (USORF)', 'CHARLESTON, UNITED STATES (USCHS)', 'JACKSONVILLE, UNITED STATES (USJAX)',
                    'CAUCEDO, DOMINICAN REPUBLIC (DOCAU)']
    path = "https://www.msc.com/search-schedules?agencyPath=bra"
    driver.get(path)
    time.sleep(15)
    try:
        driver.find_element_by_id("onetrust-accept-btn-handler").click()
        time.sleep(5)
        driver.find_element_by_class_name("js-reject").click()
        time.sleep(5)
        driver.find_element_by_id("ctl00_ctl00_ucNewsetterSignupPopup_btnReject").click()
        time.sleep(5)
    except:
        driver.delete_all_cookies()
    try:
        driver.find_element_by_css_selector("#whatsNewModal > div > a.button.button-tertiary").click()
        time.sleep(5)
    except:
        time.sleep(2)
    flag = 0
    driver, flag = import_export_data(driver, scheduled_list1, scheduled_list2, carrier_name, carrier_code, country, flag)
    driver, flag = import_export_data(driver, scheduled_list2, scheduled_list1, carrier_name, carrier_code, country, flag)
    return driver


def msc_uruguay(driver):
    carrier_name = 'MSC'
    carrier_code = '37702'
    country = 'URUGUAY'
    port_code = ' '
    uruguay_path = "https://www.msc.com/arrivals-departures?agencyPath=ury"
    driver.get(uruguay_path)
    time.sleep(20)
    try:
        driver.find_element_by_class_name("js-reject").click()
    except:
        time.sleep(2)
    try:
        driver.find_element_by_id("onetrust-accept-btn-handler").click()
    except:
        time.sleep(4)
    time.sleep(2)
    driver.find_element_by_id("ctl00_ctl00_plcMain_plcMain_txtSearch_TextField").click()
    time.sleep(2)
    input_field = driver.find_element_by_id("ctl00_ctl00_plcMain_plcMain_txtSearch_TextField")
    input_field.send_keys(country)
    time.sleep(3)
    driver.find_element_by_css_selector("#ui-id-1>li.country.ui-menu-item>a").click()     # dropdown click
    time.sleep(4)
    driver.find_element_by_id("ctl00_ctl00_plcMain_plcMain_lnkSearchButton").click()      # search click
    time.sleep(10)
    driver.find_element_by_css_selector("#ctl00_ctl00_plcMain_plcMain_tabControlArrivals > a").click() # click arrivals
    time.sleep(10)
    all_data = driver.find_elements_by_css_selector("#PanelArrivals > section:nth-child(2) > table > tbody > tr")
    for item in all_data:
        port = item.find_element_by_css_selector("td:nth-child(1)").text
        for name, code in port_code_dict.items():
            if name == port:
                port_code = code
        port_list.append(port)
        port_code_list.append(port_code)
        vessel = item.find_element_by_css_selector("td:nth-child(2)").text
        vessel_list.append(vessel)
        voyage = item.find_element_by_css_selector("td:nth-child(3)").text
        voyageimp_list.append(voyage)
        voyageexp_list.append(' ')
        arrival = item.find_element_by_css_selector("td:nth-child(5)").text
        try:
            arrival = datetime.strptime(arrival, '%a, %d %b %Y %Hh').strftime('%Y-%m-%d')
        except:
            arrival = ''
        arrival_list.append(arrival)
        depart_list.append(' ')
        country_list.append(country)
        carrier_name_list.append(carrier_name)
        carrier_code_list.append(carrier_code)
    return driver


def msc(filename):
    today_date = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f').strftime('%d %b %Y')
    driver = getDriver()
    driver = msc_argentina(driver)
    driver = msc_brazil(driver)
    driver = msc_uruguay(driver)
    driver.close()
    df = pd.DataFrame()
    df['CARRIER'] = carrier_name_list
    df['CARRIER CODE'] = carrier_code_list
    df['COUNTRY'] = pd.Series(country_list)
    df['PORT'] = pd.Series(port_list)
    df['PORT CODE'] = pd.Series(port_code_list)
    df['VESSEL'] = pd.Series(vessel_list)
    df['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df['ARRIVAL'] = pd.Series(arrival_list)
    df['DEPART'] = pd.Series(depart_list)
    df['LAST EXECUTED'] = today_date
    df.to_excel(filename, index=False)
