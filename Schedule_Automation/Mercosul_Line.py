import os
import sys
import time
from datetime import timedelta, datetime
import pandas as pd
from selenium import webdriver
from fake_useragent import UserAgent
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementClickInterceptedException, ElementNotInteractableException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from threading import Lock
from concurrent.futures import ThreadPoolExecutor

# lock = Lock()
carrier_name_list, carrier_code_list, country_list, port_list, port_code_list, vessel_list, voyageimp_list, voyageexp_list, arrival_list, depart_list = [], [], [], [], [], [], [], [], [], []
schedule_country = ['Argentina', 'Brazil', 'Uruguay']
Argentina_port_list = ['BUENOS AIRES', 'BAHIA BLANCA', 'PERTO MADRYN', 'ROSARIO', 'USHUAIA', 'ZARATE']
Brazil_port_list = ['IMBITUBA', 'ITAGUAI', 'ITAJAI', 'ITAPOA', 'MANAUS', 'NAVEGANTES', 'PARANAGUA', 'PECEM',
                    'RIO GRANDE', 'RIO DE JANEIRO', 'SALVADOR', 'SANTOS', 'SUAPE', 'VILA DO CONDE', 'VITORIA']
Uruguay_port_list = ['MONTEVIDEO']
port_code_dict = {'BUENOS AIRES': 'BUE', 'BAHIA BLANCA': 'BHI', 'PERTO MADRYN': 'PMY', 'ROSARIO': 'ROS',
                  'USHUAIA': 'USH', 'ZARATE': 'ZAE', 'MONTEVIDEO': 'MVD',
                  'IMBITUBA': 'IBB', 'ITAGUAI': 'IGI', 'ITAJAI': 'ITJ', 'ITAPOA': 'IOA', 'MANAUS': 'MAO',
                  'NAVEGANTES': 'NVT', 'PARANAGUA': 'PNG', 'PECEM': 'PEC', 'RIO GRANDE': 'RIG', 'RIO DE JANEIRO': 'RIO',
                  'SANTOS': 'SSZ', 'SALVADOR': 'SSA', 'SUAPE': 'SUA', 'VILA DO CONDE': 'VCO', 'VITORIA': 'VIX'}


# initialising Chrome Driver
def getDriver():
    chrome_options = Options()
    ua = UserAgent()
    user_agent = ua.random
    chrome_options.add_argument(f'user-agent={user_agent}')
    prefs = {'profile.managed_default_content_settings.images': 2}
    chrome_options.add_experimental_option("prefs", prefs)
    chrome_options.headless = True
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--ignore-ssl-errors')
    chrome_path = os.getcwd() + "\\chromedriver.exe"
    driver = webdriver.Chrome(chrome_path, options=chrome_options)
    driver.set_window_position(-10000, 0)
    return driver


def fetching_data(table, flag, dummy_vessel_list, dummy_voyage_list):
    port_name, port_code, country = '', '', ''
    carrier_name = 'MERCOSUL LINE'
    carrier_code = '94633'
    ports_dates = table.find_elements_by_css_selector("tbody")
    for each_port_date in ports_dates:
        ports = each_port_date.find_elements_by_css_selector("tr > th > span.left > p:nth-child(1)")
        for each_port in ports:
            port_name = str(each_port.text).replace('ITAJAÍ', 'ITAJAI')
        dates = each_port_date.find_elements_by_css_selector("tr > td > p:nth-child(3)")
        for each_date, vess, voy in zip(dates, dummy_vessel_list, dummy_voyage_list):
            if str(each_date.text) == '...':
                arrival = ' '
            else:
                arrival = datetime.strptime(str(each_date.text).split(' - ')[0], '%d/%m/%Y').strftime('%Y-%m-%d')
            carrier_name_list.append(carrier_name)
            carrier_code_list.append(carrier_code)
            vessel_list.append(vess)
            port_list.append(port_name)
            if flag == 0:
                voyageimp_list.append(' ')
                voyageexp_list.append(voy)
            elif flag == 1:
                voyageimp_list.append(voy)
                voyageexp_list.append(' ')
            if port_name in Argentina_port_list:
                country = 'ARGENTINA'
            elif port_name in Brazil_port_list:
                country = 'BRAZIL'
            elif port_name in Uruguay_port_list:
                country = 'URUGUAY'
            country_list.append(country)
            for name, code in port_code_dict.items():
                if name == port_name:
                    port_code = code
            port_code_list.append(port_code)
            arrival_list.append(arrival)
            depart_list.append(' ')
    flag = 1
    return flag


def mercosul(filename):
    today_date = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f').strftime('%d %b %Y')
    driver = getDriver()
    path = "https://www.mercosul-line.com.br/servicos/rotas/programacao"
    driver.get(path)
    time.sleep(2)
    driver.find_element_by_css_selector("#rcc-confirm-button").click()
    time.sleep(2)
    driver.find_element_by_id("Plata").click()
    time.sleep(2)
    tables = driver.find_elements_by_css_selector("#__next > div > div > section > div > div > div > div > table")
    flag = 0
    for each_table in tables:
        dummy_vessel_list, dummy_voyage_list = [], []
        vessel_voyages = each_table.find_elements_by_css_selector("thead > tr > th")
        for each_vess_voy in vessel_voyages:
            vessel_voyage = each_vess_voy.text
            if vessel_voyage == '':
                continue
            voyage = str(vessel_voyage).split('\n')[-1]
            vessel = str(vessel_voyage).replace('\n', '').split(voyage)[0].replace('L.', 'LOG IN')
            dummy_vessel_list.append(vessel)
            dummy_voyage_list.append(voyage)
        if flag == 0:
            flag = fetching_data(each_table, flag, dummy_vessel_list, dummy_voyage_list)
        elif flag == 1:
            flag = fetching_data(each_table, flag, dummy_vessel_list, dummy_voyage_list)
    driver.close()

    df = pd.DataFrame()
    df['CARRIER'] = carrier_name_list
    df['CARRIER CODE'] = carrier_code_list
    df['COUNTRY'] = pd.Series(country_list)
    df['PORT'] = pd.Series(port_list)
    df['PORT CODE'] = pd.Series(port_code_list)
    df['VESSEL NAME'] = pd.Series(vessel_list)
    df['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df['ARRIVAL DATE'] = pd.Series(arrival_list)
    df['DEPARTURE DATE'] = pd.Series(depart_list)
    df['LAST EXECUTED'] = today_date
    df.to_excel(filename, index=False)
