import csv
import os
import sys
import time
from datetime import timedelta, datetime
import pandas as pd
import tabula
from openpyxl import Workbook
from selenium import webdriver
from fake_useragent import UserAgent
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementClickInterceptedException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from threading import Lock
from concurrent.futures import ThreadPoolExecutor

#lock = Lock()

carrier_name_list, carrier_code_list, country_list, port_list, port_code_list, voyageimp_list, voyageexp_list, vessel_list, arrival_list, depart_list = [], [], [], [], [], [], [], [], [], []

Argentina_port = ['ZARATE']
Brazil_port = ['ITAGUAI', 'SANTOS', 'PARANAGUA', 'ITAPOA', 'NAVEGANTES', 'ITAJAI']
Uruguay_port = ['MONTEVIDEO']
port_code_dict = {'BUENOS AIRES': 'BUE', 'MONTEVIDEO': 'MVD', 'ITAGUAI': 'IGI', 'ITAJAI': 'ITJ', 'ITAPOA': 'IOA',
                  'NAVEGANTES': 'NVT', 'PARANAGUA': 'PNG', 'RIO GRANDE': 'RIG', 'RIO DE JANEIRO': 'RIO',
                  'SANTOS': 'SSZ'}


# initialising Chrome Driver
def getDriver():
    chrome_options = Options()
    ua = UserAgent()
    user_agent = ua.random
    chrome_options.add_argument(f'user-agent={user_agent}')
    prefs = {'profile.managed_default_content_settings.images': 2}
    chrome_options.add_experimental_option("prefs", prefs)
    chrome_options.headless = True
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--ignore-ssl-errors')
    chrome_path = os.getcwd() + "\\chromedriver.exe"
    driver = webdriver.Chrome(chrome_path, options=chrome_options)
    driver.set_window_position(-10000, 0)
    return driver


def arr_dep_formatting(arrival_date, depart_date):
    today = datetime.today()
    current_month = today.month
    year = today.year
    arr_mon_split = str(arrival_date).replace('*', '').split('/')
    dep_mon_split = str(depart_date).replace('*', '').split('/')
    arr_month = arr_mon_split[0]
    dep_month = dep_mon_split[0]
    month = ['01', '02', '03', '04', '05', '06']
    if current_month >= 10:
        if arr_month in month and dep_month in month:
            year_new = str(year + 1)
            arrival = arr_mon_split[-1] + '/' + arr_mon_split[0] + '/' + year_new
            depart = dep_mon_split[-1] + '/' + dep_mon_split[0] + '/' + year_new
        elif arr_month in month:
            year_new = str(year + 1)
            arrival = arr_mon_split[-1] + '/' + arr_mon_split[0] + '/' + year_new
            depart = dep_mon_split[-1] + '/' + dep_mon_split[0] + '/' + str(year)
        elif dep_month in month:
            year_new = str(year + 1)
            arrival = arr_mon_split[-1] + '/' + arr_mon_split[0] + '/' + str(year)
            depart = dep_mon_split[-1] + '/' + dep_mon_split[0] + '/' + year_new
        else:
            arrival = arr_mon_split[-1] + '/' + arr_mon_split[0] + '/' + str(year)
            depart = dep_mon_split[-1] + '/' + dep_mon_split[0] + '/' + str(year)
    else:
        arrival = arr_mon_split[-1] + '/' + arr_mon_split[0] + '/' + str(year)
        depart = dep_mon_split[-1] + '/' + dep_mon_split[0] + '/' + str(year)
    arrival = datetime.strptime(str(arrival), '%d/%m/%Y').strftime('%Y-%m-%d')
    depart = datetime.strptime(str(depart), '%d/%m/%Y').strftime('%Y-%m-%d')
    return arrival, depart


def fetching_ves_voy(vess_voy):
    voyage_imp, voyage_exp = '', ''
    ves_voy = vess_voy.split(" ")
    voyage = ves_voy[-1]
    if len(voyage) > 6 and '-' not in str(voyage):
        if str(voyage)[5].__contains__('W') or str(voyage)[5].__contains__('A') or str(voyage)[5].__contains__('S'):
            voyage_imp = voyage
        else:
            voyage_exp = voyage
    else:
        if str(voyage).__contains__('W') or str(voyage).__contains__('A') or str(voyage).__contains__('S'):
            voyage_imp = voyage
        else:
            voyage_exp = voyage
    vessel = vess_voy.split(voyage)[0].strip(' ')
    return vessel, voyage_imp, voyage_exp


def evergreen(filename):
    carrier_name = 'EVERGREEN'
    carrier_code = '45330'
    new_port_list, new_arr, new_dep = [], [], []
    vess_voy, port_code = '', ''
    today_date = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f').strftime('%d %b %Y')
    driver = getDriver()
    path = "https://www.shipmentlink.com/tvs2/jsp/TVS2_LongTermMenu.jsp?type=L"
    driver.get(path)
    time.sleep(2)
    driver.find_element_by_css_selector("#mainMenu2>table>tbody>tr>td>table:nth-child(1)>tbody>tr:nth-child(5)>td>a").click()
    time.sleep(2)
    driver.find_element_by_css_selector("#treeMenu25>table>tbody>tr>td:nth-child(2)>table>tbody>tr:nth-child(2)>td:nth-child(1)>a").click()
    time.sleep(2)
    window_after = driver.window_handles[1]
    driver.switch_to.window(window_after)
    table = driver.find_element_by_class_name("f13tabn1")
    tr_values = table.find_elements_by_css_selector("tr")
    ports = table.find_elements_by_css_selector("tr:nth-child(1)>td")
    for port in ports:
        if str(port.text) != " ":
            new_port_list.append(port.text)
    for val in tr_values:
        new_arr, new_dep = [], []
        values = str(val.text).replace('\n', ' ')
        if values.__contains__('/'):
            val_split = values.split(' ARR DEP ')
            vess_voy = val_split[0]
            arr_dep = val_split[1].split(' ')
            for arr in range(0, len(arr_dep), 2):
                new_arr.append(arr_dep[arr])
                new_dep.append(arr_dep[arr+1])
        for port, arr, dep in zip(new_port_list, new_arr, new_dep):
            if str(arr) == '---':
                arrival, depart = '', ''
            else:
                arrival, depart = arr_dep_formatting(arr, dep)
            if port in Argentina_port:
                for name, code in port_code_dict.items():
                    if name == port:
                        port_code = code
                country = 'ARGENTINA'
                vessel, voyage_imp, voyage_exp = fetching_ves_voy(vess_voy)
                country_list.append(country)
                port_list.append(port)
                port_code_list.append(port_code)
                vessel_list.append(vessel)
                voyageimp_list.append(voyage_imp)
                voyageexp_list.append(voyage_exp)
                arrival_list.append(arrival)
                depart_list.append(depart)
            elif port in Brazil_port:
                for name, code in port_code_dict.items():
                    if name == port:
                        port_code = code
                country = 'BRAZIL'
                vessel, voyage_imp, voyage_exp = fetching_ves_voy(vess_voy)
                country_list.append(country)
                port_list.append(port)
                port_code_list.append(port_code)
                vessel_list.append(vessel)
                voyageimp_list.append(voyage_imp)
                voyageexp_list.append(voyage_exp)
                arrival_list.append(arrival)
                depart_list.append(depart)
            elif port in Uruguay_port:
                for name, code in port_code_dict.items():
                    if name == port:
                        port_code = code
                country = 'URUGUAY'
                vessel, voyage_imp, voyage_exp = fetching_ves_voy(vess_voy)
                country_list.append(country)
                port_list.append(port)
                port_code_list.append(port_code)
                vessel_list.append(vessel)
                voyageimp_list.append(voyage_imp)
                voyageexp_list.append(voyage_exp)
                arrival_list.append(arrival)
                depart_list.append(depart)
            else:
                continue
            carrier_name_list.append(carrier_name)
            carrier_code_list.append(carrier_code)
    driver.close()

    df = pd.DataFrame()
    df['CARRIER'] = carrier_name_list
    df['CARRIER CODE'] = carrier_code_list
    df['COUNTRY'] = pd.Series(country_list)
    df['PORT'] = pd.Series(port_list)
    df['PORT CODE'] = pd.Series(port_code_list)
    df['VESSEL NAME'] = pd.Series(vessel_list)
    df['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df['ARRIVAL DATE'] = pd.Series(arrival_list)
    df['DEPARTURE DATE'] = pd.Series(depart_list)
    df['LAST EXECUTED'] = today_date
    df.to_excel(filename, index=False)
