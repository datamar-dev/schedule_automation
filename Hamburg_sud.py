import os
import sys
import time
from datetime import timedelta, datetime
import pandas as pd
from selenium import webdriver
from fake_useragent import UserAgent
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException, ElementClickInterceptedException, \
    StaleElementReferenceException
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC
from threading import Lock
from concurrent.futures import ThreadPoolExecutor


BrPl_port_list = ['ARBUE', 'ARPMY', 'ARROS', 'ARZAE', 'BRFOR', 'BRIOA', 'Itapoa (SC) BRIOA', 'BRIBB', 'BRITJ', 'BRIGI',
                  'BRMAO', 'BRNVT', 'BRPNG', 'BRPEC', 'BRRIG', 'BRRIO', 'BRSSA', 'BRSSZ', 'BRSUA', 'BRVLC', 'BRVIX', 'UYMVD']
Foreign_port_list = ['ESALG', 'MATNG', 'COCTG', 'ESCAR', 'JMKIN', 'USMSY', 'USHOU', 'MXATM', 'MXVER', 'PAMIT', 'DOMAN',
                     'MXZLO', 'ZAZBA', 'SGSIN' 'HKHKG', 'KRPUS', 'CNSHA', 'CNNGB', 'CNSHK', 'CNYTN', 'MYPKG', 'NLRTM',
                     'GBLON', 'DEHAM', 'BEANR', 'FRLEH', 'PACTB', 'PECLL', 'USPEF', 'ECGYE', 'CLARI', 'CLANF', 'CLSAI',
                     'CLSVE', 'PTSIE', 'DEBRV', 'USNYC', 'USPHL', 'USORF', 'USCHS', 'USJAX', 'DOCAU']
Br_port_list = ['BRFOR', 'BRIOA', 'Itapoa (SC) BRIOA', 'BRIBB', 'BRITJ', 'BRIGI', 'BRMAO', 'BRNVT', 'BRPNG', 'BRPEC',
                'BRRIG', 'BRRIO', 'BRSSA', 'BRSSZ', 'BRSUA', 'BRVLC', 'BRVIX']
Pl_port_list = ['ARBUE', 'ARPMY', 'ARROS', 'ARZAE', 'UYMVD']
port_code_dict = {'ARBUE': 'BUE', 'ARPMY': 'PMY', 'ARROS': 'ROS', 'ARUSH': 'USH', 'ARZAE': 'ZAE',
                  'BRFOR': 'FOR', 'BRIBB': 'IBB', 'BRIGI': 'IGI', 'BRITJ': 'ITJ', 'BRMAO': 'MAO',
                  'BRNVT': 'NVT', 'BRPNG': 'PNG', 'BRPEC': 'PEC', 'BRRIO': 'RIO', 'BRRIG': 'RIG',
                  'BRSSA': 'SSA', 'BRSSZ': 'SSZ', 'BRSUA': 'SUA', 'BRVCO': 'VCO', 'BRVIX': 'VIX', 'UYMVD': 'MVD'}
carrier_name_list, carrier_code_list, country_list, port_list, port_code_list, vessel_list, voyageimp_list, voyageexp_list, arrival_list, depart_list = [], [], [], [], [], [], [], [], [], []


# initialising Chrome Driver
def getDriver():
    chrome_options = Options()
    ua = UserAgent()
    user_agent = ua.random
    chrome_options.add_argument(f'user-agent={user_agent}')
    prefs = {'profile.managed_default_content_settings.images': 2}
    chrome_options.add_experimental_option("prefs", prefs)
    # chrome_options.headless = True
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--ignore-ssl-errors')
    chrome_path = os.getcwd() + "\\chromedriver.exe"
    driver = webdriver.Chrome(chrome_path, options=chrome_options)
    # driver.set_window_position(-10000, 0)
    return driver


def fetching_from_port(driver, port_name1):
    input_from = driver.find_element_by_xpath(
        "/html/body/main/div/div[1]/div/form/div[2]/div[1]/div/div[2]/div/div[1]/span/span/input[1]")
    input_from.clear()
    time.sleep(2)
    input_from.send_keys(port_name1)
    time.sleep(4)
    try:
        driver.find_element_by_xpath("/html/body/span[1]/table/tbody/tr/td[1]").click()
    except:
        try:
            driver.find_element_by_xpath("/html/body/span[3]/table/tbody/tr/td[1]").click()
        except NoSuchElementException:
            return driver
        time.sleep(2)
    return driver


def fetching_to_port(driver, port_name2):
    input_to = driver.find_element_by_xpath(
        "/html/body/main/div/div[1]/div/form/div[2]/div[1]/div/div[2]/div/div[2]/span/span/input[1]")
    try:
        input_to.clear()
        input_to.send_keys(port_name2)
    except StaleElementReferenceException:
        time.sleep(2)
    time.sleep(4)
    try:
        driver.find_element_by_xpath("/html/body/span[1]/table/tbody/tr/td[1]").click()
    except:
        try:
            driver.find_element_by_xpath("/html/body/span[2]/table/tbody/tr/td[1]").click()
        except:
            try:
                driver.find_element_by_xpath("/html/body/span[4]/table/tbody/tr/td[1]").click()
            except NoSuchElementException:
                return driver
    time.sleep(2)
    return driver


def fetching_data(driver, port_list1, port_list2, counter, carrier_name, carrier_code):
    for port_key1 in port_list1:
        driver = fetching_from_port(driver, port_key1)
        time.sleep(2)
        #time.sleep(2000)
        for port_key2 in port_list2:
            driver = fetching_to_port(driver, port_key2)
            time.sleep(2)
            driver.find_element_by_xpath(
                "/html/body/main/div/div[1]/div/form/div[2]/div[1]/div/div[7]/div[1]/button").click()
            time.sleep(10)
            try:
                alert_text = driver.find_element_by_xpath("/html/body/main/div/div[1]/div/form/div[1]/div/ul/li").text
                if alert_text == 'No results found.':
                    continue
            except:
                all_data = driver.find_elements_by_xpath(
                    "/html/body/main/div/div[1]/div/form/div[3]/div/div[1]/div/div[2]/div/div/table/tbody")
                for data in all_data:
                    try:
                        voyage = data.find_element_by_xpath("tr[2]/td[6]/div/a").text.split('/')[0]
                    except:
                        voyage = ''
                    if counter == 0:
                        new_port = port_key1.upper()
                        voyageexp_list.append(voyage)
                        voyageimp_list.append(' ')
                    elif counter == 1:
                        new_port = port_key2.upper()
                        voyageexp_list.append(' ')
                        voyageimp_list.append(voyage)
                    elif counter == 3:
                        new_port = port_key1.upper()
                        voyageexp_list.append(' ')
                        voyageimp_list.append(voyage)
                    else:
                        new_port = port_key2.upper()
                        voyageexp_list.append(voyage)
                        voyageimp_list.append(' ')
                    if new_port == 'MONTEVIDEO':
                        country_list.append('URUGUAY')
                    elif new_port == 'BUENOS AIRES' or new_port == 'PUERTO MADRYN' or new_port == 'ROSARIO' or new_port == 'ZARATE':
                        country_list.append('ARGENTINA')
                    else:
                        country_list.append('BRAZIL')
                    for port_code_key, port_code_value in port_code_dict.items():
                        if new_port == port_code_key:
                            port_code_list.append(port_code_value)
                    port_list.append(new_port)
                    try:
                        vessel = data.find_element_by_xpath("tr[1]/td[4]/div/a").text.split('/')[0]
                    except:
                        vessel = ''
                    vessel_list.append(vessel.upper())
                    try:
                        arrival = data.find_element_by_xpath("tr[2]/td[5]").text
                    except:
                        arrival = ' '
                    arrival_list.append(arrival)
                    try:
                        depart = data.find_element_by_xpath("tr[2]/td[2]").text
                    except:
                        depart = ' '
                    depart_list.append(depart)
                    carrier_name_list.append(carrier_name)
                    carrier_code_list.append(carrier_code)
    counter = counter + 1
    return driver, counter


def hamburg_sud(filename):
    carrier_name = 'HSUD'
    carrier_code = '1368'
    driver = getDriver()
    path = "https://www.hamburgsud-line.com/liner/en/liner_services/ecommerce/schedules/schedules__point_to_point/index.html"
    driver.get(path)
    time.sleep(2)
    driver.delete_all_cookies()
    cookies = driver.execute_script("return document.querySelector('#cmpwrapper').shadowRoot.querySelector('div.cmpclose>a')")
    cookies.click()
    try:
        driver.switch_to.frame(driver.find_element_by_id("ePortalMainFrame"))
        time.sleep(3)
    except NoSuchElementException:
        time.sleep(3)
    counter = 0
    driver, counter = fetching_data(driver, BrPl_port_list, Foreign_port_list, counter, carrier_name, carrier_code)
    #driver, counter = fetching_data(driver, Foreign_port_list, BrPl_port_list, counter, carrier_name, carrier_code)
    #driver, counter = fetching_data(driver, Br_port_list, Pl_port_list, counter, carrier_name, carrier_code)
    #driver, counter = fetching_data(driver, Pl_port_list, Br_port_list, counter, carrier_name, carrier_code)
    driver.close()

    df = pd.DataFrame()
    df['CARRIER'] = carrier_name_list
    df['CARRIER CODE'] = carrier_code_list
    df['COUNTRY'] = pd.Series(country_list)
    df['PORT'] = pd.Series(port_list)
    df['PORT CODE'] = pd.Series(port_code_list)
    df['VESSEL NAME'] = pd.Series(vessel_list)
    df['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df['ARRIVAL DATE'] = pd.Series(arrival_list)
    df['DEPARTURE DATE'] = pd.Series(depart_list)
    df.to_excel(filename, index=False)


hamburg_sud('Hamburg_sud.xlsx')