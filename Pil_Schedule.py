import os
import sys
import time
from datetime import timedelta, datetime
import pandas as pd
from selenium import webdriver
from fake_useragent import UserAgent
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementClickInterceptedException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from threading import Lock
from concurrent.futures import ThreadPoolExecutor

# lock = Lock()

carrier_name_list, carrier_code_list, country_list, port_list, port_code_list, vessel_list, voyageimp_list, voyageexp_list, arrival_list, depart_list = [], [], [], [], [], [], [], [], [], []
schedule_country = ['ARGENTINA', 'BRAZIL', 'URUGUAY']

Argentina_port_dict = {'Buenos Aires [ARBUE]': 'BUENOS AIRES', 'Rosario [ARROS]': 'ROSARIO'}
Brazil_port_dict = {'Itaguai [BRIGI]': 'ITAGUAI', 'Itapoa [BRIOA]': 'ITAPOA', 'Itajai [BRITJ]': 'ITAJAI',
                    'Navegantes [BRNVT]': 'NAVEGANTES', 'Paranagua [BRPNG]': 'PARANAGUA',
                    'Rio Grande [BRRIG]': 'RIO GRANDE', 'Rio De Janeiro [BRRIO]': 'RIO DE JANEIRO',
                    'Rio De Janeiro [BRRJO]': 'RIO DE JANEIRO', 'Sao Paulo [BRSAO]': 'SAO PAULO',
                    'Sao Paulo [BRSPO]': 'SAO PAULO', 'Santos [BRSSZ]': 'SANTOS'}
Uruguay_port_dict = {'Montevideo [UYMVD]': 'MONTEVIDEO'}
port_code_dict = {'BUENOS AIRES': 'BUE', 'ROSARIO': 'ROS', 'ITAGUAI': 'IGI', 'ITAPOA': 'IOA', 'ITAJAI': 'ITJ',
                  'NAVEGANTES': 'NVT', 'PARANAGUA': 'PNG', 'RIO GRANDE': 'RIG', 'RIO DE JANEIRO': 'RIO',
                  'SAO PAULO': 'SPO', 'SANTOS': 'SSZ', 'MONTEVIDEO': 'MVD'}
port_check_list = []


# initialising Chrome Driver
def getDriver():
    chrome_options = Options()
    ua = UserAgent()
    user_agent = ua.random
    chrome_options.add_argument(f'user-agent={user_agent}')
    prefs = {'profile.managed_default_content_settings.images': 2}
    chrome_options.add_experimental_option("prefs", prefs)
    chrome_options.headless = True
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--ignore-ssl-errors')
    chrome_path = os.getcwd() + "\\chromedriver.exe"
    driver = webdriver.Chrome(chrome_path, options=chrome_options)
    driver.set_window_position(-10000, 0)
    return driver


def looping_data(driver, country, port_key, port_value, carrier_name, carrier_code):
    port_code = ' '
    driver.find_element_by_css_selector("#by_port_chosen").click()
    time.sleep(1)
    ports = driver.find_elements_by_css_selector("#by_port_chosen > div > ul > li")
    for port_link in ports:
        if str(port_link.text) == port_key:
            port_link.click()
            time.sleep(2)
            break
    driver.find_element_by_xpath(
        "/html/body/div[1]/div/div[3]/div/div/div/div/div[2]/div[1]/form/div/div[1]/div[2]/div[3]/div/a").click()
    time.sleep(2)
    try:
        error_text = driver.find_element_by_class_name("ui-pnotify-title").text
        if str(error_text) == 'Error!':
            port_check_list.append(port_value)
            return
    except:
        time.sleep(2)
    table_data = driver.find_elements_by_css_selector("#table_byport>tbody>tr")
    for data in table_data:
        port_check_list.append(port_value)
        try:
            vessel = data.find_element_by_css_selector("td:nth-child(5)").text
        except:
            continue
        try:
            voyage = data.find_element_by_css_selector("td:nth-child(3)").text
        except:
            voyage = ' '
        try:
            arrival = data.find_element_by_css_selector("td:nth-child(6)").text
            depart = data.find_element_by_css_selector("td:nth-child(8)").text
            arrival = datetime.strptime(str(arrival).split(' ')[0], '%d-%b-%Y').strftime('%Y-%m-%d')
            depart = datetime.strptime(str(depart).split(' ')[0], '%d-%b-%Y').strftime('%Y-%m-%d')
        except:
            continue
        port_list.append(port_value)
        for name, code in port_code_dict.items():
            if name == port_value:
                port_code = code
        port_code_list.append(port_code)
        vessel_list.append(vessel)
        if 'E' in voyage or 'N' in voyage or 'R' in voyage:
            voyageimp_list.append(' ')
            voyageexp_list.append(voyage)
        else:
            voyageimp_list.append(voyage)
            voyageexp_list.append(' ')
        arrival_list.append(arrival)
        carrier_name_list.append(carrier_name)
        carrier_code_list.append(carrier_code)
        depart_list.append(depart)
        country_list.append(country)


def pil(filename):
    carrier_name = 'PIL'
    carrier_code = '64576'
    today_date1 = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f').strftime('%d %b %Y')
    today_date = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f')
    driver = getDriver()
    path = "https://www.pilship.com/en-our-schedules-pil-pacific-international-lines/117.html"
    driver.get(path)
    driver.delete_all_cookies()
    time.sleep(2)
    from_date = today_date.strftime('%d-%b-%Y')
    to_date = (today_date + timedelta(days=90)).strftime('%d-%b-%Y')
    date_from = driver.find_element_by_id("by_port_date_from")
    date_from.clear()
    date_from.send_keys(from_date)
    time.sleep(2)
    date_to = driver.find_element_by_id("by_port_date_to")
    date_to.clear()
    date_to.send_keys(to_date)
    time.sleep(2)
    for country in schedule_country:
        if country == 'ARGENTINA':
            schedule_port = Argentina_port_dict
        elif country == 'BRAZIL':
            schedule_port = Brazil_port_dict
        else:
            schedule_port = Uruguay_port_dict
        for port_key, port_value in schedule_port.items():
            looping_data(driver, country, port_key, port_value, carrier_name, carrier_code)
    for country in schedule_country:
        if country == 'ARGENTINA':
            schedule_port = Argentina_port_dict
        elif country == 'BRAZIL':
            schedule_port = Brazil_port_dict
        else:
            schedule_port = Uruguay_port_dict
        for new_port_key, new_port_value in schedule_port.items():
            if new_port_value not in port_check_list:
                looping_data(driver, country, new_port_key, new_port_value, carrier_name, carrier_code)
    driver.close()

    df = pd.DataFrame()
    df['CARRIER'] = carrier_name_list
    df['CARRIER CODE'] = carrier_code_list
    df['COUNTRY'] = pd.Series(country_list)
    df['PORT'] = pd.Series(port_list)
    df['PORT CODE'] = pd.Series(port_code_list)
    df['VESSEL NAME'] = pd.Series(vessel_list)
    df['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df['ARRIVAL DATE'] = pd.Series(arrival_list)
    df['DEPARTURE DATE'] = pd.Series(depart_list)
    df['LAST EXECUTED'] = today_date1
    df.to_excel(filename, index=False)
