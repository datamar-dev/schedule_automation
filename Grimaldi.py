import os
import re
import sys
import time
from datetime import timedelta, datetime
import camelot
import openpyxl
from openpyxl import Workbook
import csv
import pandas as pd
import tabula
from tabula import read_pdf
from selenium import webdriver
from PyPDF2 import PdfFileReader, PdfFileWriter
from fake_useragent import UserAgent
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementClickInterceptedException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from threading import Lock
from concurrent.futures import ThreadPoolExecutor

# lock = Lock()
carrier_name_list, carrier_code_list, country_list, port_list, port_code_list, vessel_list, voyageimp_list, voyageexp_list, arrival_list, depart_list = [], [], [], [], [], [], [], [], [], []

Argentina_port = ['Zarate']
Brazil_port = ['Suape', 'Vitoria', 'Rio De Janeiro', 'Santos', 'Paranagua']
Uruguay_port = ['Montevideo']
port_code_dict = {'BAHIA BLANCA': 'BHI', 'BUENOS AIRES': 'BUE', 'CONCEPCION DEL URUGUAY': 'CON', 'CORRIENTES': 'CNQ',
                  'ROSARIO': 'ROS', 'ZARATE': 'ZAR', 'BELEM': 'BEL', 'FORTALEZA': 'FOR', 'IMBITUBA': 'IBB',
                  'ITAGUAI': 'IGI', 'ITAPOA': 'IOA', 'ITAJAI': 'ITJ', 'ITAQUI': 'ITQ', 'MACAPA': 'MCP',
                  'MACEIO': 'MCZ', 'MANAUS': 'MAO', 'NATAL': 'NAT', 'NAVEGANTES': 'NVT', 'PARANAGUA': 'PNG', 'PECEM': 'PEC',
                  'PORTO ALEGRE': 'POA', 'RECIFE': 'REC', 'RIO DE JANEIRO': 'RIO', 'RIO DE JANIERO': 'RIO', 'RIO GRANDE': 'RIG',
                  'SALVADOR DI BAHIA': 'SSA', 'SAO PAULO': 'SPO', 'SANTANA': 'SAN', 'SANTAREM': 'STM', 'SANTOS': 'SSZ',
                  'SAO FRANCISCO DO SUL': 'SFS', 'SUAPE': 'SUA', 'VILA DO CONDE': 'VCO', 'VITORIA': 'VIX', 'MONTEVIDEO': 'MVD'}


# initialising Chrome Driver
def getDriver():
    chrome_options = Options()
    ua = UserAgent()
    user_agent = ua.random
    chrome_options.add_argument(f'user-agent={user_agent}')
    prefs = {'profile.managed_default_content_settings.images': 2}
    chrome_options.add_experimental_option("prefs", prefs)
    chrome_options.headless = True
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--ignore-ssl-errors')
    chrome_path = os.getcwd() + "\\chromedriver.exe"
    driver = webdriver.Chrome(chrome_path, options=chrome_options)
    driver.set_window_position(-10000, 0)
    return driver


def arr_dep_formatting(arrival_date, depart_date):
    today = datetime.today()
    current_month = today.month
    year = today.year
    arr_month = str(arrival_date).split('/')[-1]
    dep_month = str(depart_date).split('/')[-1]
    month = ['01', '02', '03', '04', '05', '06']
    if current_month >= 10:
        if arr_month in month and dep_month in month:
            year_new = str(year + 1)
            arrival = arrival_date + '/' + year_new
            depart = depart_date + '/' + year_new
        elif arr_month in month:
            year_new = str(year + 1)
            arrival = arrival_date + '/' + year_new
            depart = depart_date + '/' + str(year)
        elif dep_month in month:
            year_new = str(year + 1)
            arrival = arrival_date + '/' + str(year)
            depart = depart_date + '/' + year_new
        else:
            arrival = arrival_date + '/' + str(year)
            depart = depart_date + '/' + str(year)
    else:
        arrival = arrival_date + '/' + str(year)
        depart = depart_date + '/' + str(year)
    arrival = datetime.strptime(str(arrival), '%d/%m/%Y').strftime('%Y-%m-%d')
    depart = datetime.strptime(str(depart), '%d/%m/%Y').strftime('%Y-%m-%d')
    return arrival, depart


def looping(tr_values, ves_voy_value, port_value, eta_value, etd_value, vessel, voyage, carrier_name, carrier_code):
    port_code = ''
    for values in tr_values:
        new_col1 = values.find_elements_by_css_selector("td")
        if len(new_col1) < 2:
            continue
        new_col2 = values.find_element_by_css_selector("td:nth-child(2)")
        if new_col2.text == 'ETA':
            continue
        elif len(new_col1) == 5:
            col2 = values.find_elements_by_css_selector(ves_voy_value)
            for val2 in col2:
                if val2.text.isalnum():
                    voyage = val2.text
                else:
                    vessel = val2.text
        else:
            col1 = values.find_elements_by_css_selector(port_value)
            col2 = values.find_elements_by_css_selector(eta_value)
            col3 = values.find_elements_by_css_selector(etd_value)
            for port, eta, etd in zip(col1, col2, col3):
                if port.text in Argentina_port:
                    country = 'ARGENTINA'
                elif port.text in Brazil_port:
                    country = 'BRAZIL'
                elif port.text in Uruguay_port:
                    country = 'URUGUAY'
                else:
                    continue
                port = str(port.text).upper()
                for name, code in port_code_dict.items():
                    if name == port:
                        port_code = code
                country_list.append(country)
                port_list.append(port)
                eta = str(eta.text).replace(' (*)', '')
                etd = str(etd.text).replace(' (*)', '')
                if eta == '' or etd == '':
                    arrival, depart = ' ', ' '
                else:
                    arrival, depart = arr_dep_formatting(eta, etd)
                carrier_name_list.append(carrier_name)
                carrier_name_list.append(carrier_code)
                arrival_list.append(arrival)
                depart_list.append(depart)
                voyageimp_list.append(voyage)
                voyageexp_list.append(' ')
                vessel_list.append(vessel.upper())
                port_code_list.append(port_code)


def grimaldi(filename):
    carrier_name = 'GRIMALDI'
    carrier_code = '1260'
    today_date = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f').strftime('%d %b %Y')
    driver = getDriver()
    path = "https://www.net.grimaldi.co.uk/GNET45/Pages_GAtlas/WFContainerTracking"
    driver.get(path)
    time.sleep(2)
    driver.find_element_by_id("xctrl_Menu").click()
    time.sleep(2)
    driver.find_element_by_css_selector("#ui-accordion-xctrl_MenuAccordion-header-0").click()
    time.sleep(2)
    driver.find_element_by_css_selector("#ui-accordion-xctrl_MenuAccordion-panel-0 > li:nth-child(4) > a").click()
    time.sleep(2)
    window_after = driver.window_handles[1]
    driver.switch_to.window(window_after)
    table = driver.find_element_by_class_name("tableschedule")
    tr_values = table.find_elements_by_css_selector("tr")
    vessel, voyage = ' ', ' '
    ves_voy_value = "td:nth-child(2)"
    port_value = "td:nth-child(1)"
    eta_value = "td:nth-child(2)"
    etd_value = "td:nth-child(3)"
    looping(tr_values, ves_voy_value, port_value, eta_value, etd_value, vessel, voyage, carrier_name, carrier_code)
    ves_voy_value = "td:nth-child(3)"
    port_value = "td:nth-child(1)"
    eta_value = "td:nth-child(4)"
    etd_value = "td:nth-child(5)"
    looping(tr_values, ves_voy_value, port_value, eta_value, etd_value, vessel, voyage, carrier_name, carrier_code)
    ves_voy_value = "td:nth-child(4)"
    port_value = "td:nth-child(1)"
    eta_value = "td:nth-child(6)"
    etd_value = "td:nth-child(7)"
    looping(tr_values, ves_voy_value, port_value, eta_value, etd_value, vessel, voyage, carrier_name, carrier_code)
    ves_voy_value = "td:nth-child(5)"
    port_value = "td:nth-child(1)"
    eta_value = "td:nth-child(8)"
    etd_value = "td:nth-child(9)"
    looping(tr_values, ves_voy_value, port_value, eta_value, etd_value, vessel, voyage, carrier_name, carrier_code)
    driver.close()

    df = pd.DataFrame()
    df['CARRIER'] = carrier_name_list
    df['CARRIER CODE'] = carrier_code_list
    df['COUNTRY'] = pd.Series(country_list)
    df['PORT'] = pd.Series(port_list)
    df['PORT CODE'] = pd.Series(port_code_list)
    df['VESSEL NAME'] = pd.Series(vessel_list)
    df['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df['ARRIVAL DATE'] = pd.Series(arrival_list)
    df['DEPARTURE DATE'] = pd.Series(depart_list)
    df['LAST EXECUTED'] = today_date
    df.to_excel(filename, index=False)
