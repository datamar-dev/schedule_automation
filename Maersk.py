import os
import sys
import time
from datetime import timedelta, datetime
import pandas as pd
from selenium import webdriver
from fake_useragent import UserAgent
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementClickInterceptedException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from threading import Lock
from concurrent.futures import ThreadPoolExecutor

# lock = Lock()
carrier_name_list1, carrier_code_list1, carrier_name_list2, carrier_code_list2, carrier_name_list3, carrier_code_list3 = [], [], [], [], [], []
country_list, port_list, port_code_list, vessel_list, voyageexp_list, voyageimp_list, arrival_list, depart_list = [], [], [], [], [], [], [], []
schedule_country = ['Argentina', 'Brazil', 'Uruguay']
Argentina_port_list = ['Buenos Aires', 'Bahia Blanca', 'Mar del Plata', 'Puerto Deseado', 'Puerto Madryn', 'Rosario',
                       'Ushuaia', 'ZARATE']
Brazil_port_list = ['Imbituba', 'Itaguai Sepetiba', 'Itajai', 'Itapoa', 'Manaus', 'Navegantes', 'Paranagua', 'Pecem',
                    'Rio Grande', 'Rio de Janeiro', 'Salvador', 'Santos', 'Suape', 'Vila do Conde', 'Vitoria']
Uruguay_port_list = ['Montevideo']
port_code_dict = {'Buenos Aires': 'BUE', 'Bahia Blanca': 'BHI', 'Mar del Plata': 'MDQ', 'Puerto Deseado': 'PUD',
                  'Puerto Madryn': 'PMY', 'Rosario': 'ROS', 'Ushuaia': 'USH', 'ZARATE': 'ZAE', 'Montevideo': 'MVD',
                  'Imbituba': 'IBB', 'Itaguai Sepetiba': 'IGI', 'Itajai': 'ITJ', 'Itapoa': 'IOA', 'Manaus': 'MAO',
                  'Navegantes': 'NVT', 'Paranagua': 'PNG', 'Pecem': 'PEC', 'Rio Grande': 'RIG', 'Rio de Janeiro': 'RIO',
                  'Santos': 'SSZ', 'Salvador': 'SSA', 'Suape': 'SUA', 'Vila do Conde': 'VCO', 'Vitoria': 'VIX'}


# initialising Chrome Driver
def getDriver():
    chrome_options = Options()
    ua = UserAgent()
    user_agent = ua.random
    chrome_options.add_argument(f'user-agent={user_agent}')
    prefs = {'profile.managed_default_content_settings.images': 2}
    chrome_options.add_experimental_option("prefs", prefs)
    chrome_options.headless = True
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    chrome_options.add_argument('--ignore-certificate-errors')
    chrome_options.add_argument('--ignore-ssl-errors')
    chrome_path = os.getcwd() + "\\chromedriver.exe"
    driver = webdriver.Chrome(chrome_path, options=chrome_options)
    driver.set_window_position(-10000, 0)
    return driver


def moving_next_date(driver, today_date):
    date_split = today_date.split(' ')
    driver.find_element_by_css_selector("#app>div.schedules-view>span>form>div:nth-child(3)>span>div>div>div>div>input").click()
    time.sleep(1)
    month = driver.find_element_by_class_name("mx-btn-current-month")
    driver.execute_script("arguments[0].innerText = '" + date_split[1] + "'", month)
    time.sleep(1)
    year = driver.find_element_by_class_name("mx-btn-current-year")
    driver.execute_script("arguments[0].innerText = '" + date_split[2] + "'", year)
    time.sleep(1)
    dates = driver.find_elements_by_class_name("cell")
    for date in dates:
        if str(date.text) == date_split[0]:
            date.click()
            time.sleep(1)
            break


def fetching_data(driver, country, port):
    carrier_name1, carrier_code1 = 'MAERSK', '61839'
    carrier_name2, carrier_code2 = 'SEALAND', '66976'
    carrier_name3, carrier_code3 = 'SAFMARINE', '9172'
    try:
        all_data = driver.find_elements_by_class_name("results__schedule")
        port_code = ''
        for data in all_data:
            vessel = data.find_element_by_css_selector("div.results__schedule--cell.departure > div > div > a").text
            voyage = (data.find_element_by_css_selector(
                "div.results__schedule--cell.departure > div > div:nth-child(2)").text).split('|')
            #if vessel in vessel_list and voyage[0].strip(' ') in voyageexp_list and voyage[1].strip(' ') in voyageimp_list\
                    #or vessel in vessel_list and voyage[1].strip(' ') in voyageexp_list and voyage[0].strip(' ') in voyageimp_list:
                #continue
            country_list.append(country.upper())
            for name, code in port_code_dict.items():
                if name == port:
                    port_code = code
            port_code_list.append(port_code)
            port_list.append(port.upper())
            vessel_list.append(str(vessel).upper())
            if 'E' in voyage[0] or 'N' in voyage[0] or 'R' in voyage[0]:
                voyageexp_list.append(str(voyage[0].strip(' ')).upper())
                voyageimp_list.append(str(voyage[1].strip(' ')).upper())
            elif 'E' in voyage[1] or 'N' in voyage[1] or 'R' in voyage[1]:
                voyageexp_list.append(str(voyage[1].strip(' ')).upper())
                voyageimp_list.append(str(voyage[0].strip(' ')).upper())
            else:
                voyageexp_list.append(str(voyage[0].strip(' ')).upper())
                voyageimp_list.append(str(voyage[1].strip(' ')).upper())
            arrival = data.find_element_by_css_selector("div.results__schedule--cell.vessel > div.font--default--bold").text
            arrival = datetime.strptime(str(arrival), '%d %b %Y %H:%M').strftime('%Y-%m-%d')
            arrival_list.append(arrival)
            depart = data.find_element_by_css_selector("div.results__schedule--cell.transit > div.font--default--bold").text
            depart = datetime.strptime(str(depart), '%d %b %Y %H:%M').strftime('%Y-%m-%d')
            depart_list.append(depart)
            carrier_name_list1.append(carrier_name1)
            carrier_code_list1.append(carrier_code1)
            carrier_name_list2.append(carrier_name2)
            carrier_code_list2.append(carrier_code2)
            carrier_name_list3.append(carrier_name3)
            carrier_code_list3.append(carrier_code3)
        return driver
    except:
        return driver


def maersk(filename):
    today_date = datetime.today()
    start_date = datetime.strptime(str(today_date), '%Y-%m-%d %H:%M:%S.%f').strftime('%d %b %Y')
    driver = getDriver()
    path = "https://www.maersk.com/schedules/portCalls"
    driver.get(path)
    time.sleep(5)
    try:
        driver.delete_all_cookies()
        driver.find_element_by_css_selector("#coiPage-1 > div.coi-banner__page-footer > button.coi-banner__accept.coi-banner__accept--fixed-margin").click()
    except:
        print("No cookies")
    time.sleep(2)
    for country in schedule_country:
        # executor.submit(country)
        # select dropdown without select option  and with input
        country_search = driver.find_element_by_id("countryRegion")
        country_search.send_keys(Keys.CONTROL, 'a')
        country_search.send_keys(Keys.DELETE)
        try:
            country_search.click()
        except ElementClickInterceptedException:
            time.sleep(1)
        country_dropdown = driver.find_element(By.XPATH, "//*[text()='"+country+"']")
        ActionChains(driver).click(on_element=country_dropdown).perform()
        time.sleep(2)

        if country == 'Argentina':
            schedule_port = Argentina_port_list
        elif country == 'Brazil':
            schedule_port = Brazil_port_list
        else:
            schedule_port = Uruguay_port_list
        for port in schedule_port:
            today_date = datetime.today()
            port_search = driver.find_element_by_id("port")
            port_search.send_keys(Keys.CONTROL, 'a')
            port_search.send_keys(Keys.DELETE)
            try:
                port_search.click()
            except ElementClickInterceptedException:
                time.sleep(1)
            port_dropdown = driver.find_element(By.XPATH, "//*[text()='"+port+"']")
            ActionChains(driver).click(on_element=port_dropdown).perform()
            time.sleep(2)
            moving_next_date(driver, start_date)
            try:
                driver.find_element_by_class_name("button.button--primary.button--block").click()
            except ElementClickInterceptedException:
                time.sleep(1)

            driver = fetching_data(driver, country, port)
            time.sleep(2)
            end_day = today_date + timedelta(days=90)
            end_date = str(end_day).split(" ")[0]
            url = driver.current_url.split('Date=')[0]
            while 1:
                next_day = today_date + timedelta(days=1)
                next_date = str(next_day).split(" ")[0]
                new_url = url+"Date="+next_date+""
                driver.get(new_url)
                time.sleep(2)
                driver = fetching_data(driver, country, port)
                today_date = next_day
                if next_date == end_date:
                    break
    driver.close()

    today_date = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f').strftime('%d %b %Y')
    df1 = pd.DataFrame()
    df1['CARRIER'] = carrier_name_list1
    df1['CARRIER CODE'] = carrier_code_list1
    df1['COUNTRY'] = pd.Series(country_list)
    df1['PORT'] = pd.Series(port_list)
    df1['PORT CODE'] = pd.Series(port_code_list)
    df1['VESSEL NAME'] = pd.Series(vessel_list)
    df1['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df1['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df1['ARRIVAL DATE'] = pd.Series(arrival_list)
    df1['DEPARTURE DATE'] = pd.Series(depart_list)
    df1['LAST EXECUTED'] = today_date
    df1.to_excel(filename, index=False)

    df2 = pd.DataFrame()
    df2['CARRIER'] = carrier_name_list2
    df2['CARRIER CODE'] = carrier_code_list2
    df2['COUNTRY'] = pd.Series(country_list)
    df2['PORT'] = pd.Series(port_list)
    df2['PORT CODE'] = pd.Series(port_code_list)
    df2['VESSEL NAME'] = pd.Series(vessel_list)
    df2['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df2['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df2['ARRIVAL DATE'] = pd.Series(arrival_list)
    df2['DEPARTURE DATE'] = pd.Series(depart_list)
    df2['LAST EXECUTED'] = today_date
    df2.to_excel('Sealand.xlsx', index=False)

    df3 = pd.DataFrame()
    df3['CARRIER'] = carrier_name_list3
    df3['CARRIER CODE'] = carrier_code_list3
    df3['COUNTRY'] = pd.Series(country_list)
    df3['PORT'] = pd.Series(port_list)
    df3['PORT CODE'] = pd.Series(port_code_list)
    df3['VESSEL NAME'] = pd.Series(vessel_list)
    df3['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df3['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df3['ARRIVAL DATE'] = pd.Series(arrival_list)
    df3['DEPARTURE DATE'] = pd.Series(depart_list)
    df3['LAST EXECUTED'] = today_date
    df3.to_excel('Safmarine.xlsx', index=False)
