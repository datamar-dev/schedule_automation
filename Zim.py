import re
import os
import pdfplumber
import pandas as pd
from datetime import datetime


carrier_name_list, carrier_code_list, country_list, port_list, port_code_list, vessel_list, voyageimp_list, voyageexp_list, arrival_list, depart_list = [], [], [], [], [], [], [], [], [], []
scheduled_country = ['ARGENTINA', 'BRAZIL', 'URUGUAY']
Argentina_dict = {'BUENOS AIRES': 'Buenos Aires.pdf', 'LAS PALMAS': 'Las Palmas.pdf', 'ROSARIO': 'Rosario.pdf', 'ZARATE': 'Zarate.pdf'}
Brazil_dict = {'ILHEUS': 'Ilheus.pdf', 'IMBITUBA': 'Imbituba.pdf', 'ITAGUAI': 'Itaguai.pdf', 'ITAJAI': 'Itajai.pdf',
               'ITAPOA': 'Itapoa.pdf', 'ITAQUI': 'Itaqui.pdf', 'NAVEGANTES': 'Navegantes.pdf', 'PARANAGUA': 'Paranagua.pdf',
               'PECEM': 'Pecem.pdf', 'PORTO ALEGRE': 'Porto Alegre.pdf', 'RECIFE': 'Recife.pdf', 'RIO DE JANEIRO': 'Rio De Janeiro.pdf',
               'RIO GRANDE': 'Rio Grande.pdf', 'SALVADOR': 'Salvador.pdf', 'SANTOS': 'Santos.pdf', 'SAO FRANCISCO DO SUL': 'Sao Francisco Do Sul.pdf',
               'SEPETIBA': 'Sepetiba.pdf', 'SUAPE': 'Suape.pdf', 'VITORIA': 'Vitoria.pdf'}
Uruguay_dict = {'MONTEVIDEO': 'Montevideo.pdf'}
port_code_dict = {'BUENOS AIRES': 'BUE', 'LAS PALMAS': 'LPS', 'ROSARIO': 'ROS', 'ZARATE': 'ZAE', 'MONTEVIDEO': 'MVD',
                  'ILHEUS': 'IOS', 'IMBITUBA': 'IBB', 'ITAGUAI': 'IGI', 'ITAQUI': 'ITQ', 'ITAJAI': 'ITJ', 'ITAPOA': 'IOA',
                  'NAVEGANTES': 'NVT', 'PARANAGUA': 'PNG', 'RIO GRANDE': 'RIG', 'RIO DE JANEIRO': 'RIO',
                  'SANTOS': 'SSZ', 'PECEM': 'PEC', 'PORTO ALEGRE': 'POA', 'RECIFE': 'REC', 'SALVADOR': 'SSA',
                  'SAO FRANCISCO DO SUL': 'SFS', 'SEPETIBA': 'SPB', 'SUAPE': 'SUA', 'VITORIA': 'VIX'}


def zim(filename):
    carrier_name = 'ZIM'
    carrier_code = '1503'
    files = os.listdir(os.getcwd())
    for country in scheduled_country:
        if country == 'ARGENTINA':
            schedule_port = Argentina_dict
        elif country == 'BRAZIL':
            schedule_port = Brazil_dict
        else:
            schedule_port = Uruguay_dict
        for port, file in schedule_port.items():
            if file in files:
                with pdfplumber.open(file) as pdf:
                    for page in pdf.pages:
                        text = page.extract_text()
                        for line in text.split('\n'):
                            checking = re.search(r'\d{2}-\w{3}-\d{4} \d{2}:\d{2}', line)
                            if checking:
                                # main information line filtering  and getting all data
                                check_str = checking.group()
                                depature = str(line).split(check_str)[1].strip()
                                vessel = str(line).split('(')[0].strip()
                                arr_date = str(check_str).split(' ')[0]
                                dep_date = str(depature).split(' ')[0]
                                voyage = str(line).split(str(arr_date))[0].split(' ')[-4]
                                arr_date = datetime.strptime(arr_date, '%d-%b-%Y').strftime('%Y-%m-%d')
                                dep_date = datetime.strptime(dep_date, '%d-%b-%Y').strftime('%Y-%m-%d')
                                if str(voyage).__contains__('E') or str(voyage).__contains__('R') or str(voyage).__contains__('N'):
                                    voyageexp_list.append(voyage)
                                    voyageimp_list.append(' ')
                                else:
                                    voyageimp_list.append(voyage)
                                    voyageexp_list.append(' ')
                                for p_name, p_code in port_code_dict.items():
                                    if port == p_name:
                                        port_code = p_code
                                vessel_list.append(vessel)
                                arrival_list.append(arr_date)
                                depart_list.append(dep_date)
                                port_list.append(port)
                                country_list.append(country)
                                port_code_list.append(port_code)
                                carrier_name_list.append(carrier_name)
                                carrier_code_list.append(carrier_code)

    df = pd.DataFrame()
    df['CARRIER'] = carrier_name_list
    df['CARRIER CODE'] = carrier_code_list
    df['COUNTRY'] = pd.Series(country_list)
    df['PORT'] = pd.Series(port_list)
    df['PORT CODE'] = pd.Series(port_code_list)
    df['VESSEL NAME'] = pd.Series(vessel_list)
    df['VOYAGE IMPORT'] = pd.Series(voyageimp_list)
    df['VOYAGE EXPORT'] = pd.Series(voyageexp_list)
    df['ARRIVAL DATE'] = pd.Series(arrival_list)
    df['DEPARTURE DATE'] = pd.Series(depart_list)
    today_date = datetime.strptime(str(datetime.today()), '%Y-%m-%d %H:%M:%S.%f').strftime('%d %b %Y')
    df['LAST EXECUTED'] = today_date
    df.to_excel(filename, index=False)
